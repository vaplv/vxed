# VoXel EDitor

`VxED` is a console based interactive voxel editor. Currently, it loads,
voxelizes and displays an Alias Wavefront `obj`. It can also report some
informations like the loading or the ray-casting performances as well as some
scene statistics.

## How to build

The library uses [CMake](http://www.cmake.org) and the
[RCMake](https://gitorious.org/code-plot/rcmake/) package to build. It also
depends on
[GNU Readline](http://cnswww.cns.cwru.edu/php/chet/readline/rltop.html),
[RSys](https://gitorious.org/code-plot/rsys/),
[SDL](https://www.libsdl.org/),
[VxAW](https://gitorious.org/code-plot/vxaw/),
[VxL](https://gitorious.org/code-plot/vxl), and
[VxR](https://gitorious.org/code-plot/vxr/).

First, ensure that CMake is installed on your system. Then, install the RCMake
package as well as all the aforementioned prerequisites. Finally, generate the
project from the `cmake/CMakeLists.txt` file by appending to the
`CMAKE_PREFIX_PATH` variable the install directories of its dependencies.

## Usage

Type `vxed -h` to print help on the `vxed` program. Running `vxed` without
argument launch `vxed` in command-line mode. `vxed` has 2 basic modes:

  - Command-line mode: in command-line mode, you can enter a command. Type
    `help` to print the list of available commands
  - Run mode: in run mode, you can interactively move the camera into the scene.

### Keyboard actions

#### Camera
  + **z**: Move forward
  + **s**: Move backward
  + **q**: Move left
  + **d**: Move right
  + **SPACE**: Move up
  + **c**: Move Down
  + **j**: Look down
  + **k**: Look up
  + **h**: Look left
  + **l**: Look right

#### Miscellaneous
  + **CTRL+c**: Stop run mode and go in command-line mode
  + **ESCAPE**: quit `vxed`

### Mouse actions
  + **Right button**: Free look while dragging
  + **Left button**: Move up/down/left/right while dragging
  + **Wheel up**: Move forward
  + **Wheel down**: Move backward

## License

VxED is Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr). It is a free
software released under the [OSI](https://opensource.org)-approved GPL v3+
license. You are welcome to redistribute it under certain conditions; refer to
the COPYING file for details.

