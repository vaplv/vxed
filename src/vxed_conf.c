/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxed.h"
#include "vxed_cmd.h"
#include "vxed_conf.h"

#include <rsys/logger.h>

#include <stdio.h>

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
vxed_conf_load(struct vxed* ed, const char* filename)
{
  FILE* file;
  res_T res = RES_OK;

  if(!ed || !filename || !filename[0])
    return RES_BAD_ARG;

  file = fopen(filename, "r");
  if(!file) {
    logger_print(&ed->logger, LOG_ERROR,
      "Error opening the conf file `%s'\n", filename);
    return RES_IO_ERR;
  }
  res = vxed_conf_load_stream(ed, file, filename);
  fclose(file);
  return res;
}

res_T
vxed_conf_load_stream(struct vxed* ed, FILE* stream, const char* name)
{
  struct darray_char buf;
  char* line;
  size_t iline;
  const unsigned buf_chunk = 128;
  res_T res = RES_OK;

  if(!ed || ! stream || !name)
    return RES_BAD_ARG;

  darray_char_init(ed->allocator, &buf);
  res = darray_char_resize(&buf, buf_chunk);
  if(res != RES_OK) {
    logger_print(&ed->logger, LOG_ERROR, "Unsufficient memory\n");
    goto error;
  }

  iline = 1;
  while((line = fgets
    (darray_char_data_get(&buf), (int)darray_char_size_get(&buf), stream))) {
    size_t last_char;

    while(!strrchr(line, '\n')) { /* Ensure that the whole line was read */
      res = darray_char_resize(&buf, darray_char_size_get(&buf) + buf_chunk);
      if(res != RES_OK) {
        logger_print(&ed->logger, LOG_ERROR, "Unsufficient memory\n");
        goto error;
      }
      line = darray_char_data_get(&buf);
      if(!fgets(line + strlen(line), (int)buf_chunk, stream)) /* EOF */
        break;
    }

    /* Remove heading spaces */
    while((*line == ' ' || *line == '\t') && *line != '\0')
      ++line;

    /* Remove the newline character(s) */
    last_char = strlen(line);
    while(last_char-- && (line[last_char]=='\n' || line[last_char]=='\r'));
    line[last_char + 1] = '\0';

    if(*line != '\0' && *line != '#') {
      res = vxed_cmd_set.func(ed, strtok(line, "#"), vxed_cmd_set.context);
      if(res != RES_OK) {
        logger_print(&ed->logger, LOG_WARNING,
          "%s:%lu: unknown or malformed config directive `%s'\n",
          name, (unsigned long)iline, line);
      }
    }
    ++iline;
  }
exit:
  darray_char_release(&buf);
  return res;
error:
  goto exit;
}

