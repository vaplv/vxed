/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXED_APP_H
#define VXED_APP_H

#include <rsys/rsys.h>

struct mem_allocator;
struct vxed;
struct vxed_args;

extern LOCAL_SYM res_T
vxed_create
  (struct mem_allocator* allocator, /* May be NULL <=> use default allocator */
   const struct vxed_args* args,
   struct vxed** ed);

extern LOCAL_SYM res_T
vxed_release
  (struct vxed* ed);

extern LOCAL_SYM res_T
vxed_run
  (struct vxed* ed);

#endif /* VXED_APP_H */

