/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxed_cmd_c.h"

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
vxed_cmd_func_help_show(struct vxed* ed, char* arg, void* ctx)
{
  (void)ctx;
  if(arg) {
    return vxed_cmd_exec(ed, &vxed_cmd_help_show.sub_cmds, arg);
  } else {
    static const char* help[] = {
"Generic command to show something about VxEd.\n",
"Type \"help show\" followed by a sub-command for further documentation.\n"
"\n",
"show SOMETHING\n",
"\n",
"List of show sub-commands:\n",
"\n"
    };
    const size_t nlines = sizeof(help)/sizeof(const char*);
    res_T res = RES_OK;

    res = vxed_print_text(ed, help, nlines);
    if(res != RES_OK) return res;
    vxed_cmd_print(ed, &vxed_cmd_show.sub_cmds, nlines);
    return RES_OK;
  }
}

res_T
vxed_cmd_func_help_show_camera_speed_rotation
  (struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Show the current sensitivity of the camera rotations.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_show_camera_speed_translation
  (struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Show the current sensitivity of the camera translations.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_show_copying(struct vxed* vxed, char* arg, void* ctx)
{
  (void)arg, (void)ctx;
  logger_print(&vxed->logger, LOG_OUTPUT, "%s\n", vxed_cmd_show_copying.doc);
  return RES_OK;
}

res_T
vxed_cmd_func_help_show_cvars(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Show the value of all the configuration variables.\n",
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_show_field_of_view(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Show the current horizontal field of view (in degree), i.e. the horizontal\n",
"extent of the observable scene that is seen by the camera at a given time.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_show_point_of_view(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Show the current point of view of the camera as three 3 dimensions floating\n",
"point vectors each formatted as X:Y:Z (X, Y and Z are the dimension values\n",
"along each axis). The first triplet is the camera position, the second one is\n",
"its targeted position and the last one is its up direction.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_show_print_load_perf(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Show the time spent to load the scene.\n",
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_show_print_render_perf(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Show the rendering time followed by the corresponding number of trace rays\n"
"per second for each frame.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_show_scene_info(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Show informations on the current scene as the voxelised resolutions, the depth\n",
"of the acceleration data structure or the number of voxels.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_show_video_mode(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Show the current video mode of the display window as WIDTHxHEIGHT@BPPbpp.\n",
"WIDTH and HEIGHT are the display window dimension in pixels while BPP is its\n",
"pixel depth in Bits Per Pixel\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_show_vxl_definition(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Show the default voxelization definition.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_show_vxl_LOD_min(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Show the lower range of the octree depth to clamp access to.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_show_vxl_LOD_max(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Show the upper range of the octree depth to clamp access to.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_show_vxl_rt_coherency(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Show the level ray-tracing coherency.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_show_vxl_rt_simd(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Show whether or not the ray-tracing is accelerated by the Single Instruction\n",
"Multiple Data (SIMD) instruction sets.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_show_vxr_beam_prepass(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Show whether or not the \"beam prepass\" ray-tracing optimisation is enabled.\n",
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_show_vxr_render_type(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = { "TODO\n" };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_show_warranty(struct vxed* vxed, char* arg, void* ctx)
{
  (void)arg, (void)ctx;
  logger_print(&vxed->logger, LOG_OUTPUT, "%s\n", vxed_cmd_show_warranty.doc);
  return RES_OK;
}

