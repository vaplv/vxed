/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXED_CMD
  #define VXED_CMD(Name, Doc)
#endif

VXED_CMD(clear, "Clean up the scene.")
VXED_CMD(help, "Print list of commands.")
VXED_CMD(load, "Load a new scene.")
VXED_CMD(next, "Draw one frame and challenge a new command.")
VXED_CMD(quit, "Exit vxed.")
VXED_CMD(run, "Run the rendering loop.")
VXED_CMD(screenshot, "Take a screenshot.")
VXED_CMD(set, "Evaluate an expression and affect its value to a variable.")
VXED_CMD(show, "Generic command to show something about vxed.")
VXED_CMD(source, "Load configuration file.")

#undef VXED_CMD
