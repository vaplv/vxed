/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxed_parse.h"

res_T
vxed_parse_float3(const char* str, float dst[3])
{
  char buf[128];
  int i;
  ASSERT(dst && str);

  if(strlen(str) >= sizeof(buf) - 1/*NULL char*/)
    return RES_MEM_ERR;
  strncpy(buf, str, sizeof(buf));

  FOR_EACH(i, 0, 3) {
    char* token = i == 0 ? strtok(buf, ":") : strtok(NULL, ":");
    res_T res;
    if(!token)
      return RES_BAD_ARG;
    res = vxed_str_to_float(token, dst + i);
    if(res != RES_OK)
      return res;
  }
  if(strtok(NULL, ":"))
    return RES_BAD_ARG;
  return RES_OK;
}

res_T
vxed_parse_video_mode(const char* str, unsigned vmode[3])
{
  char buf[128];
  char* tk;
  res_T res;
  ASSERT(str && vmode);

  if(strlen(str) >= sizeof(buf) - 1/*NULL char*/)
    return RES_MEM_ERR;
  strncpy(buf, str, sizeof(buf));

  tk = strtok(buf, "x");
  res = vxed_str_to_uint(tk, vmode + 0);
  if(res != RES_OK)
    return res;

  tk = strtok(NULL, "@");
  res = vxed_str_to_uint(tk, vmode + 1);
  if(res != RES_OK)
    return res;

  tk = strtok(NULL, "");
  res = vxed_str_to_uint(tk, vmode + 2);
  if(res != RES_OK)
    return res;
  return RES_OK;
}

res_T
vxed_parse_fov(const char* str, float* fov)
{
  float tmp;
  res_T res;
  ASSERT(str && fov);
  res = vxed_str_to_float(str, &tmp);
  if(res != RES_OK) return res;
  if(tmp < 30.f || tmp > 120.f) return RES_BAD_ARG;
  *fov = (float)MDEG2RAD(tmp);
  return RES_OK;
}

