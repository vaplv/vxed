/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxed.h"
#include "vxed_cmd_c.h"

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
vxed_cmd_func_help(struct vxed* ed, char* opt, void* ctx)
{
  ASSERT(ed);
  (void)ctx;

  if(opt && *opt) {
    return vxed_cmd_exec(ed, &vxed_cmd_help.sub_cmds, opt);
  } else {
    static const char* help[] = {
      "Type \"help\" followed by a command for further documentation.\n",
      "\n",
      "List of commands:\n",
      "\n"
    };
    const size_t nlines = sizeof(help)/sizeof(const char*);
    res_T res;

    res = vxed_print_text(ed, help, nlines);
    if(res != RES_OK) return res;
    vxed_cmd_print(ed, &ed->cmds, nlines);
    return RES_OK;
  }
}

res_T
vxed_cmd_func_help_clear(struct vxed* ed, char* opt, void* ctx)
{
  static const char* help[] = { "Remove all objects from the scene.\n" };
  (void)opt, (void)ctx;
  return vxed_print_text(ed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_help(struct vxed* ed, char* opt, void* ctx)
{
  (void)opt, (void)ctx;
  logger_print(&ed->logger, LOG_OUTPUT, "%s\n", vxed_cmd_help.doc);
  return RES_OK;
}

res_T
vxed_cmd_func_help_next(struct vxed* ed, char* opt, void* ctx)
{
  (void)opt, (void)ctx;
  logger_print(&ed->logger, LOG_OUTPUT, "%s\n", vxed_cmd_next.doc);
  return RES_OK;
}

res_T
vxed_cmd_func_help_quit(struct vxed* ed, char* opt, void* ctx)
{
  (void)opt, (void)ctx;
  logger_print(&ed->logger, LOG_OUTPUT, "%s\n", vxed_cmd_quit.doc);
  return RES_OK;
}

res_T
vxed_cmd_func_help_run(struct vxed* ed, char* opt, void* ctx)
{
  (void)opt, (void)ctx;
  logger_print(&ed->logger, LOG_OUTPUT, "%s\n", vxed_cmd_run.doc);
  return RES_OK;
}

res_T
vxed_cmd_func_help_load(struct vxed* ed, char* opt, void* ctx)
{
  static const char* help[] = {
"Load a new scene.\n",
"\n",
"import [-d DEFINITION] [-m <incore|oocore>] FILE\n"
"\n",
"The -m option defined the location where the voxels data are stored, i.e. in\n",
"core or out-of-core. If not defined, the memory location is incore\n"
  };
  (void)opt, (void)ctx;
  return vxed_print_text(ed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_screenshot(struct vxed* ed, char* opt, void* ctx)
{
  static const char* help[] = {
    "Save the current displayed image into a PPM image.\n",
    "\n",
    "screenshot OUT_FILENAME\n"
  };
  (void)opt, (void)ctx;
  return vxed_print_text(ed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_source(struct vxed* ed, char* opt, void* ctx)
{
  static const char* help[] = {
    "Execute the content of the file.\n",
    "\n",
    "source FILE\n"
  };
  (void)opt, (void)ctx;
  return vxed_print_text(ed, help, sizeof(help)/sizeof(const char*));
}

