/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxed.h"
#include "vxed_cmd_c.h"

/*******************************************************************************
 * Set vxl_rt_coherency sub-commands
 ******************************************************************************/
VXED_CMD_DECLARE
  (vxed_cmd_set_vxl_rt_coherency_high,
   "high",
   "Highly coherent ray-tracing.",
   vxed_cmd_func_set_vxl_rt_coherency_high,
   NULL);

VXED_CMD_DECLARE
  (vxed_cmd_set_vxl_rt_coherency_medium,
   "medium",
   "Coherent ray-tracing.",
   vxed_cmd_func_set_vxl_rt_coherency_medium,
   NULL);

VXED_CMD_DECLARE
  (vxed_cmd_set_vxl_rt_coherency_low,
   "low",
   "Poorly coherent ray-tracing.",
   vxed_cmd_func_set_vxl_rt_coherency_low,
   NULL);

VXED_CMD_DECLARE
  (vxed_cmd_set_vxl_rt_coherency_mixed,
   "mixed",
   "Adaptively select between coherent and incoherent ray-tracing.",
   vxed_cmd_func_set_vxl_rt_coherency_mixed,
   NULL);

VXED_CMD_DECLARE
  (vxed_cmd_set_vxl_rt_coherency_none,
   "none",
   "No ray-tracing coherency.",
   vxed_cmd_func_set_vxl_rt_coherency_none,
   NULL);

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
vxed_cmd_func_set_vxl_rt_coherency(struct vxed* ed, char* opt, void* ctx)
{
  (void)ctx;
  if(!opt) {
    logger_print(&ed->logger, LOG_ERROR, "Missing argument.\n");
    return RES_BAD_ARG;
  }
  return vxed_cmd_exec(ed, &vxed_cmd_set_vxl_rt_coherency.sub_cmds, opt);
}

#define DEFINE_CMD_FUNC_SET_VXL_RT_COHERENCY__(name, NAME)                     \
  res_T                                                                        \
  vxed_cmd_func_set_vxl_rt_coherency_ ## name                                  \
    (struct vxed* ed, char* opt, void* ctx)                                    \
  {                                                                            \
    ASSERT(ed);                                                                \
    (void)opt, (void)ctx;                                                      \
    ed->vxr_draw_state.rt_state.coherency = VXL_RT_COHERENCY_ ## NAME;         \
    return RES_OK;                                                             \
  }
DEFINE_CMD_FUNC_SET_VXL_RT_COHERENCY__(high, HIGH)
DEFINE_CMD_FUNC_SET_VXL_RT_COHERENCY__(medium, MEDIUM)
DEFINE_CMD_FUNC_SET_VXL_RT_COHERENCY__(low, LOW)
DEFINE_CMD_FUNC_SET_VXL_RT_COHERENCY__(mixed, MIXED)
DEFINE_CMD_FUNC_SET_VXL_RT_COHERENCY__(none, NONE)
#undef DEFINE_CMD_FUNC_SET_VXL_RT_COHERENCY__

