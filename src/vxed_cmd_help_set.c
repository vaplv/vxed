/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxed_cmd_c.h"

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
vxed_cmd_func_help_set(struct vxed* ed, char* arg, void* ctx)
{
  (void)ctx;
  if(arg && *arg) {
    return vxed_cmd_exec(ed, &vxed_cmd_help_set.sub_cmds, arg);
  } else {
    static const char* help[] = {
"Modify the VxEd config variables. These variables control the VxEd environment.\n",
"Type \"help set\" followed by a sub-command for further documentation.\n",
"\n",
"set CVAR VALUE\n",
"\n",
"List of sub-commands:\n",
"\n"
    };
    const size_t nlines = sizeof(help)/sizeof(const char*);
    res_T res = RES_OK;

    res = vxed_print_text(ed, help, nlines);
    if(res != RES_OK) return res;
    vxed_cmd_print(ed, &vxed_cmd_set.sub_cmds, nlines);
    return RES_OK;
  }
}

res_T
vxed_cmd_func_help_set_camera_speed_rotation
  (struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Control the sensitivity of the camera rotations in the display window. Its\n",
"value is a floating point number includes in [0, INFINITY).\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_set_camera_speed_translation
  (struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Control the sensitivity of the camera translations in the display window. Its\n",
"value is a floating point number includes in [0, INFINITY).\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_set_field_of_view(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Control the camera Field Of View (fov), i.e. the extent of the observable\n",
"scene that is seen by the camera at a given moment. This variable defines the\n",
"horizontal field of view in degree in [30, 120]. The vertical field of view\n",
"is computed from the horizontal field of view and the pixel ratio of the\n",
"display window:\n",
"  vertical_fov = horizontal_fov * display_height / display_width\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_set_point_of_view(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Control the current point of view of the camera, i.e. its position, the targeted\n",
"position it is looking at and its up direction (that is the direction from\n",
"the bottom to the top of the viewing volume). Each of these parameters are 3\n",
"dimensions floating point vectors formatted as X:Y:Z, where X, Y and Z are\n",
"the dimension values.\n",
"\n",
"set point_of_view POSITION [TARGET [UP]]\n",
"\n",
"POSITION must be always defined while TARGET and UP are optionals. When not\n",
"defined, their value is not updated. The \"set point_of_view\" operation may\n",
"failed if the resulting point of view is not valid; POSITION and TARGET must\n",
"not be equal and the {TARGET - POSITION} vector must not be collinear to the\n",
"UP direction.\n",
"\n",
"Examples:\n",
"  Update only the position of the camera:\n",
"    set point_of_view -0.5:1.0:0.5\n",
"\n",
"  Update the position as well as target of the camera:\n",
"    set point_of_view 0.1:10:-0.2 0.0:0.0:0.0\n",
"\n",
"  Update all the point of view parameters:\n",
"    set point_of_view -200.0:100.0:0.0 0.0:100.0:0.0 0.0:1.0:0.0\n",
"\n",
"  Invalid points of view:\n",
"    set point_of_view 0:0:0 0:0:0\n",
"    set point_of_view 0:0:1 0:0:0 0:0:1\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_set_print_load_perf(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Control the write to the VxEd output of the time spent to load the submitted\n",
"scene.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_set_print_render_perf(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Control the write to the VxEd output of the time spent to render each frame.\n",
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_set_video_mode(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Control the video mode of the display window, i.e. its width and height\n",
"dimensions in pixels and the depth of each pixel in Bit Per Pixel (bpp).\n",
"\n",
"set video_mode WIDTHxHEIGHT@BPP\n",
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_set_vxl_definition(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Control the default voxelization definition.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_set_vxl_LOD_min(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Control the lower range of the octree depth to clamp access to. If the\n",
"submitted integer is greater than the vxl_LOD_max value then both vxl_LOD_min\n",
"and vxl_LOD_max variable is set to the new value. This variable is use only\n",
"if the vxl_accel_type is \"svo\". Its value is an integer in [0, INT_MAX].\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_set_vxl_LOD_max(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Control the upper range of the octree depth to clamp access to. If the\n",
"submitted integer is less than the vxl_LOD_min value then both vxl_LOD_min\n",
"and vxl_LOD_max variable is set to the new value. This variable is use only\n",
"if the vxl_accel_type is \"svo\". Its value is an integer in [0, INT_MAX].\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_set_vxl_rt_simd(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Control the use of the Single Instruction Multiple Data (SIMD) instruction\n",
"sets to enhance the ray-tracing performances.\n",
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_set_vxr_beam_prepass(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Control the use of the \"beam prepass\" optimisation for primary rays\n",
"described by S. Laine and T. Karras in the \"Efficient Sparse Voxel\n",
"Octrees\" I3D 2010 paper. This variable is used only if vxl_accel_type is\n",
"\"svo\".\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_set_vxr_render_type(struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Control the type of rendering.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

