/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxed.h"
#include "vxed_cmd_c.h"

/*******************************************************************************
 * help set vxl_rt_coherency sub-commands
 ******************************************************************************/
VXED_CMD_DECLARE
  (vxed_cmd_help_set_vxl_rt_coherency_high,
   "high",
   "Highly coherent ray-tracing.",
   vxed_cmd_func_help_set_vxl_rt_coherency_high,
   NULL);

VXED_CMD_DECLARE
  (vxed_cmd_help_set_vxl_rt_coherency_medium,
   "medium",
   "Coherent ray-tracing.",
   vxed_cmd_func_help_set_vxl_rt_coherency_medium,
   NULL);

VXED_CMD_DECLARE
  (vxed_cmd_help_set_vxl_rt_coherency_low,
   "low",
   "Poorly coherent ray-tracing.",
   vxed_cmd_func_help_set_vxl_rt_coherency_low,
   NULL);

VXED_CMD_DECLARE
  (vxed_cmd_help_set_vxl_rt_coherency_mixed,
   "mixed",
   "Adaptively select between coherent and incoherent ray-tracing.",
   vxed_cmd_func_help_set_vxl_rt_coherency_mixed,
   NULL);

VXED_CMD_DECLARE
  (vxed_cmd_help_set_vxl_rt_coherency_none,
   "none",
   "No ray-tracing coherency.",
   vxed_cmd_func_help_set_vxl_rt_coherency_none,
   NULL);

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
vxed_cmd_func_help_set_vxl_rt_coherency(struct vxed* vxed, char* arg, void* ctx)
{
  (void)ctx;
  if(arg) {
    return vxed_cmd_exec(vxed, &vxed_cmd_help_set_vxl_rt_coherency.sub_cmds, arg);
  } else {
    static const char* help[] = {
"Hint on the coherency of primary rays, i.e. rays starting from the camera. The\n",
"ray coherency is impacted by the voxel size; the more the voxels are large with\n",
"respect to the pixel footprint, the more the rays are coherent.\n",
"\n",
"Performances are greatly impacted by this parameter. Best performances are\n",
"obtained with high coherency, but setting the coherency to high while the\n",
"rays are actually incoherent or poorly coherent reduces the rendering\n",
"performances.\n",
"\n",
"set vxl_rt_coherency LEVEL\n",
"\n",
"Type \"help set vxl_rt_coherency\" followed by a sub-command for further\n",
"documentation.\n",
"\n",
"List of sub-commands:\n",
"\n"
    };
    const size_t nlines = sizeof(help)/sizeof(const char*);
    res_T res;
    res = vxed_print_text(vxed, help, nlines);
    if(res != RES_OK) return res;
    vxed_cmd_print(vxed, &vxed_cmd_help_set_vxl_rt_coherency.sub_cmds, nlines);
    return RES_OK;
  }
}

res_T
vxed_cmd_func_help_set_vxl_rt_coherency_high
  (struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"The primary rays are highly coherent, i.e. the scene voxelisation is very coarse \n",
"with respect to the point of view.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_set_vxl_rt_coherency_medium
  (struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"The primary rays are coherent, i.e. each rendered voxel is visible by several\n",
"pixels.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_set_vxl_rt_coherency_low
  (struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"The primary rays are coherent a little bit, i.e. each voxel is visible by only\n",
"few pixels\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_set_vxl_rt_coherency_mixed
  (struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Some primary rays are coherent while others are incoherent i.e. some voxels\n",
"cover several pixels while others are small. Actually, the mixed coherency\n",
"should be well suited in many situations, even though all visible voxels are\n",
"small, since primary rays are coherent during the traversal of the firsts\n",
"level of the voxelised data structure.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

res_T
vxed_cmd_func_help_set_vxl_rt_coherency_none
  (struct vxed* vxed, char* arg, void* ctx)
{
  static const char* help[] = {
"Disable the ray-tracing coherency optimisations.\n"
  };
  (void)arg, (void)ctx;
  return vxed_print_text(vxed, help, sizeof(help)/sizeof(const char*));
}

