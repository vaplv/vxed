/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXED_VIEW_H
#define VXED_VIEW_H

#include "vxed.h"
#include <rsys/rsys.h>

struct vxed_view;
struct vx;
struct vxed;
struct vxr_camera;
struct window;

BEGIN_DECLS

VXED_API res_T
vxed_view_create
  (struct vxed* ed,
   struct vxed_view** view);

VXED_API void
vxed_view_ref_get
  (struct vxed_view* view);

VXED_API void
vxed_view_ref_put
  (struct vxed_view* view);

VXED_API res_T
vxed_view_setup
  (struct vxed_view* view,
   const float pos[3],
   const float target[3],
   const float up[3],
   const float proj_ratio,
   const float fov_x);

VXED_API void
vxed_view_translate
  (struct vxed_view* view,
   const float trans[3]);

VXED_API void
vxed_view_rotate
  (struct vxed_view* view,
   const float rot[3]);

VXED_API res_T
vxed_view_set_look_at
  (struct vxed_view* view,
   const float pos[3],
   const float target[3],
   const float up[3]);

VXED_API void
vxed_view_get_look_at
  (const struct vxed_view* view,
   float pos[3],
   float target[3],
   float up[3]);

VXED_API res_T
vxed_view_set_fov
  (struct vxed_view* view,
   const float fov_x); /* In radians */

VXED_API float
vxed_view_get_fov
  (struct vxed_view* view);

VXED_API res_T
vxed_view_get_vxr_camera
  (const struct vxed_view* view,
   struct vxr_camera** cam);

/* Link the view to a given window, i.e. `view' listen `win' signals. For
 * instance, on `win' resize, `view' redefines its projection ratio. Note that
 * A view can be attached to only one window. If a view is already attached it
 * is detached from the previous window priorly to its new attachement */
VXED_API void
vxed_view_attach_to_window
  (struct vxed_view* view,
   struct vxed_window* window);

VXED_API void
vxed_view_detach_from_window
  (struct vxed_view* view);

END_DECLS

#endif /* VIEW_H */

