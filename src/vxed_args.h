/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXED_ARGS_H
#define VXED_ARGS_H

#include <rsys/math.h>
#include <vx/vxl.h>

struct vxed_args {
  const char* conf_filename; /* Name of the config file to use */
  unsigned long definition; /* # voxels along the X, Y and Z axis */
  enum vxl_mem_location mem_location; /* Memory location of the voxels */
  unsigned video_mode[3]; /* Width, height and bits per pixel */
  unsigned nthreads_hint; /* Hint on the number of threads to use */
  float view_position[3]; /* Position of the point of view */
  float view_target[3]; /* Targeted position of the point of view */
  float view_up[3]; /* Up vector */
  float view_fov; /* View field of view in radian */
  int verbose; /* Verbosity level */
  char* model; /* Model to load */
};

static const struct vxed_args VXED_ARGS_DEFAULT = {
  "./.vxedrc", /* conf filename */
  512, /* Definition */
  VXL_MEM_INCORE, /* Memory location */
  {800, 600, 32}, /* Video mode */
  ~0u, /* nthreads_hint */
  { 0.f, 0.f, -100.f }, /* view position */
  { 0.f, 0.f, 0.f }, /* view target */
  { 0.f, 1.f, 0.f }, /* view up */
  (float)MDEG2RAD(70.0), /* Field of view */
  0, /* verbose */
  NULL, /* model */
};

extern LOCAL_SYM res_T
vxed_args_parse
  (struct vxed_args* args,
   int* quit, /* Quit the application ? */
   const int argc,
   char* argv[]);

#endif /* VXED_ARGS_H */

