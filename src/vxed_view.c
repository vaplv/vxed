/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxed.h"
#include "vxed_view.h"
#include "vxed_window.h"

#include <rsys/float3.h>
#include <rsys/float33.h>
#include <rsys/mem_allocator.h>
#include <rsys/ref_count.h>
#include <vx/vxr.h>

#include <string.h>

struct vxed_view {
  struct vxr_camera* vxr_cam;

  float axis_x[3], axis_y[3], axis_z[3];
  float zoom;
  float position[3];
  float up[3];
  char vxr_cam_is_outdated;

  struct vxed_window* win;
  vxed_window_cb_T cb_on_window_mode_update;

  ref_T ref;
  struct vxed* ed;
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T
create_vxr_camera(struct vxed_view* view)
{
  struct vxr_camera* cam = NULL;
  res_T res;
  ASSERT(view);

  res = vxr_camera_create(view->ed->vxr, &cam);
  if(res != RES_OK) {
    logger_print(&view->ed->logger, LOG_ERROR,
      "Could not create the VoXel Renderer camera\n");
    return res;
  }
  if(view->vxr_cam) VXR(camera_ref_put(view->vxr_cam));
  view->vxr_cam = cam;
  return RES_OK;
}

static void
on_window_mode_update(struct vxed_window* win, void* ctx)
{
  float ratio;
  struct vxed_view* view = ctx;
  res_T res;
  ASSERT(win && ctx);
  ratio = vxed_window_get_pixel_ratio(win);
  res = vxr_camera_set_proj_ratio(view->vxr_cam, ratio);
  if(res != RES_OK) {
    logger_print(&view->ed->logger, LOG_ERROR,
      "Could not set the camera projection ratio `%.2f'\n", ratio);
  }
}

static void
vxed_view_release(ref_T* ref)
{
  struct vxed_view* view = CONTAINER_OF(ref, struct vxed_view, ref);
  ASSERT(ref);
  vxed_view_detach_from_window(view);
  if(view->vxr_cam) VXR(camera_ref_put(view->vxr_cam));
  MEM_RM(view->ed->allocator, view);
}

/*******************************************************************************
 * Local function
 ******************************************************************************/
VXED_DEFINE_REF_FUNCS(vxed_view)

res_T
vxed_view_create(struct vxed* ed, struct vxed_view** out_view)
{
  struct vxed_view* view = NULL;
  res_T res = RES_OK;
  ASSERT(ed && out_view);

  view = MEM_CALLOC(ed->allocator, 1, sizeof(struct vxed_view));
  if(!view) {
    logger_print(&ed->logger, LOG_ERROR, "Could not create the view\n");
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&view->ref);
  view->ed = ed;
  CLBK_INIT(&view->cb_on_window_mode_update);
  CLBK_SETUP(&view->cb_on_window_mode_update, on_window_mode_update, view);

  res = create_vxr_camera(view);
  if(res != RES_OK) goto error;

exit:
  *out_view = view;
  return res;
error:
  if(view) {
    vxed_view_ref_put(view);
    view = NULL;
  }
  goto exit;
}

res_T
vxed_view_setup
  (struct vxed_view* view,
   const float pos[3],
   const float target[3],
   const float up[3],
   const float proj_ratio,
   const float fov_x)
{
  struct vxr_camera_desc cam_desc;
  res_T res = RES_OK;
  ASSERT(view && pos && target && up);

  f3_set(cam_desc.position, pos);
  f3_set(cam_desc.target, target);
  f3_set(cam_desc.up, up);
  cam_desc.proj_ratio = proj_ratio;
  cam_desc.fov_x = fov_x;
  res = vxr_camera_setup(view->vxr_cam, &cam_desc);
  if(res != RES_OK) {
    logger_print(&view->ed->logger, LOG_ERROR,
      "Couldn't setup VoXel Renderer camera:\n"
      "  position: %f %f %f\n"
      "  target: %f %f %f\n"
      "  up: %f %f %f\n"
      "  projection ratio: %f\n"
      "  horizontal field of view: %f\n",
      SPLIT3(pos),
      SPLIT3(target),
      SPLIT3(up),
      proj_ratio,
      fov_x);
    goto error;
  }
  res = vxed_view_set_look_at(view, pos, target, up);
  if(res != RES_OK) goto error;

exit:
  return res;
error:
  goto exit;
}

void
vxed_view_translate(struct vxed_view* view, const float trans[3])
{
  float tmp0[3], tmp1[3];
  ASSERT(view && trans);

  if(f3_eq_eps(trans, f3_splat(tmp0, 0), 1.e-6f))
    return; /* Ignore ~0 translation */

  f3_mulf(tmp0, view->axis_x, trans[0]);
  f3_add(tmp0, tmp0, f3_mulf(tmp1, view->up, trans[1]));
  f3_add(tmp0, tmp0, f3_mulf(tmp1, view->axis_z, trans[2]));
  f3_add(view->position, view->position, tmp0);
  view->vxr_cam_is_outdated = 1;
}

void
vxed_view_rotate(struct vxed_view* view, const float rot[3])
{
  float rot_X[9], rot_Up[9], mat[9];
  ASSERT(view && rot);

  if(f3_eq_eps(rot, f3_splat(rot_X, 0), 1.e-6f))
    return; /* Ignore ~0 translation */

  f33_rotation_pitch(rot_X, rot[0]);
  f33_rotation_axis_angle(rot_Up, view->up, rot[1]);
  f33_set_row(mat, view->axis_x, 0);
  f33_set_row(mat, view->axis_y, 1);
  f33_set_row(mat, view->axis_z, 2);

  f33_mulf33(mat, rot_X, mat); /* Rotate around the local X axis */
  f33_mulf33(mat, mat, rot_Up); /* Rotate around the global Up axis */

  f33_row(view->axis_x, mat, 0);
  f33_row(view->axis_y, mat, 1);
  f33_row(view->axis_z, mat, 2);

  if(f3_dot(view->axis_y, view->up) < 0.f)
    f3_minus(view->up, view->up);

  view->vxr_cam_is_outdated = 1;
}

res_T
vxed_view_set_look_at
  (struct vxed_view* view,
   const float pos[3],
   const float target[3],
   const float up[3])
{
  float axis_x[3], axis_y[3], axis_z[3];
  float zoom;
  res_T res = RES_OK;
  ASSERT(view && pos && target && up);

  f3_sub(axis_z, target, pos);
  zoom = f3_normalize(axis_z, axis_z);
  if(zoom <= 0.f) {
    res = RES_BAD_ARG;
    goto error;
  }
  f3_cross(axis_x, axis_z, up);
  if(f3_normalize(axis_x, axis_x) <= 0.f) {
    res = RES_BAD_ARG;
    goto error;
  }
  f3_cross(axis_y, axis_x, axis_z);
  if(f3_normalize(axis_y, axis_y) <= 0.f) {
    res = RES_BAD_ARG;
    goto error;
  }

  view->zoom = zoom;
  f3_set(view->position, pos);
  f3_set(view->up, up);
  f3_set(view->axis_x, axis_x);
  f3_set(view->axis_y, axis_y);
  f3_set(view->axis_z, axis_z);
  view->vxr_cam_is_outdated = 1;

exit:
  return res;
error:
  logger_print(&view->ed->logger, LOG_ERROR,
    "Couldn't setup the view:\n"
    "  position: %.2f %.2f %.2f\n"
    "  target: %.2f %.2f %.2f\n"
    "  up: %.2f %.2f %.2f\n",
    SPLIT3(pos),
    SPLIT3(target),
    SPLIT3(up));
  goto exit;
}

void
vxed_view_get_look_at
  (const struct vxed_view* view,
   float pos[3],
   float target[3],
   float up[3])
{
  ASSERT(view && pos && target && up);
  f3_set(pos, view->position);
  f3_add(target, pos, f3_mulf(target, view->axis_z, view->zoom));
  f3_set(up, view->up);
}

res_T
vxed_view_set_fov(struct vxed_view* view, const float fov_x)
{
  res_T res;
  ASSERT(view);

  res = vxr_camera_set_fov(view->vxr_cam, fov_x);
  if(res != RES_OK) {
    logger_print(&view->ed->logger, LOG_ERROR,
      "Could not set the horizontal field of view to `%.2f' degree\n",
      MRAD2DEG(fov_x));
  }
  return res;
}

float
vxed_view_get_fov(struct vxed_view* view)
{
  float fov;
  res_T res;
  ASSERT(view);
  res = vxr_camera_get_fov(view->vxr_cam, &fov);
  if(res != RES_OK) {
    logger_print(&view->ed->logger, LOG_ERROR,
      "Could not retrieve the camera field of view\n");
    return FLT_MAX;
  }
  return fov;
}

res_T
vxed_view_get_vxr_camera(const struct vxed_view* view, struct vxr_camera** cam)
{
  float pos[3], tgt[3], up[3];
  res_T res;
  ASSERT(view && cam);

  if(!view->vxr_cam_is_outdated) {
    *cam = view->vxr_cam;
    return RES_OK;
  }

  vxed_view_get_look_at(view, pos, tgt, up);
  res = vxr_camera_look_at(view->vxr_cam, pos, tgt, up);
  if(res != RES_OK) {
    logger_print(&view->ed->logger, LOG_ERROR,
      "Could not setup the camera point of view:\n"
      "  position: %f %f %f\n"
      "  target: %f %f %f\n"
      "  up: %f %f %f\n",
    SPLIT3(pos), SPLIT3(tgt), SPLIT3(up));
    return res;
  }
  *cam = view->vxr_cam;
  return RES_OK;
}

void
vxed_view_attach_to_window(struct vxed_view* view, struct vxed_window* win)
{
  ASSERT(view && win);
  vxed_view_detach_from_window(view);
  vxed_window_ref_get(win);
  view->win = win;
  vxed_window_listen
    (win, VXED_WINDOW_SIG_UPDATE_MODE, &view->cb_on_window_mode_update);
  on_window_mode_update(win, view);
}

void
vxed_view_detach_from_window(struct vxed_view* view)
{
  ASSERT(view);
  if(!view->win)
    return;
  CLBK_DISCONNECT(&view->cb_on_window_mode_update);
  vxed_window_ref_put(view->win);
  view->win = NULL;
}

