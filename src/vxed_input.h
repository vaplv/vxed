/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * VoXel EDitor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * VoXel EDitor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with VoXel EDitor. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXED_INPUT_H
#define VXED_INPUT_H

#include "vxed.h"

#include <rsys/rsys.h>

#include <string.h>
#include <SDL.h>

enum vxed_input_key { /* List of input keys */
  VXED_KEY_MOVE_LEFT,
  VXED_KEY_MOVE_RIGHT,
  VXED_KEY_MOVE_UP,
  VXED_KEY_MOVE_DOWN,
  VXED_KEY_MOVE_FORWARD,
  VXED_KEY_MOVE_BACKWARD,
  VXED_KEY_LOOK_UP,
  VXED_KEY_LOOK_DOWN,
  VXED_KEY_LOOK_LEFT,
  VXED_KEY_LOOK_RIGHT,
  VXED_KEY_QUIT,
  VXED_KEY_STOP,
  VXED_KEYS_COUNT__
};

enum vxed_input_button {
  VXED_BUTTON_LOOK,
  VXED_BUTTON_MOVE,
  VXED_BUTTONS_COUNT__
};

/* Helper macro used as syntactic sugar */
#define VXED_BUTTON_FLAG(Button) BIT(Button)

struct vxed_events { /* Events that occur in a VxEd window */
  int keys[VXED_KEYS_COUNT__];
  int buttons; /* Combination of VXED_BUTTONS_FLAG(enum vxed_input_button) */
  int mouse_dx;
  int mouse_dy;
};

/* Helper macro that cleans-up any registered events */
#define VXED_EVENTS_CLEAR(Events) memset(Events, 0, sizeof(struct vxed_events));

struct vxed_input_key_desc {
  SDLKey key; /* Keybord key */
  SDLMod mod; /* Key modifiers */
};

/* Default key binding on VxEd window events */
static const struct vxed_input_key_desc VXED_INPUT_DEFAULT_KEY_BINDING
[VXED_KEYS_COUNT__] = {
  { SDLK_q, KMOD_NONE }, /* Move left */
  { SDLK_d, KMOD_NONE }, /* Move right */
  { SDLK_SPACE, KMOD_NONE }, /* Move up */
  { SDLK_c, KMOD_NONE }, /* Move down */
  { SDLK_z, KMOD_NONE }, /* Move forward */
  { SDLK_s, KMOD_NONE }, /* Move backward */
  { SDLK_k, KMOD_NONE }, /* Look up */
  { SDLK_j, KMOD_NONE }, /* Look down */
  { SDLK_h, KMOD_NONE }, /* Look left */
  { SDLK_l, KMOD_NONE }, /* Look right */
  { SDLK_c, KMOD_CTRL }, /* Quit */
  { SDLK_ESCAPE, KMOD_NONE } /* Stop */
};

/* Default mouse button binding on VxEd window events */
static const uint8_t VXED_INPUT_DEFAULT_BUTTON_BINDING
[VXED_BUTTONS_COUNT__] = {
  SDL_BUTTON_RIGHT, /* Look */
  SDL_BUTTON_LEFT, /* Move */
};

struct vxed;
struct vxed_input;

/*******************************************************************************
 * VxEd input API
 ******************************************************************************/
BEGIN_DECLS

VXED_API res_T
vxed_input_create
  (struct vxed* ed,
   const struct vxed_input_key_desc key_binding[VXED_KEYS_COUNT__],
   const uint8_t button_binding[VXED_BUTTONS_COUNT__],
   struct vxed_input** input);

VXED_API void
vxed_input_ref_get
  (struct vxed_input* input);

VXED_API void
vxed_input_ref_put
  (struct vxed_input* input);

/* Poll for all currently pending events */
VXED_API res_T
vxed_input_poll_events
  (struct vxed_input* input,
   struct vxed_events* events);

VXED_API res_T
vxed_input_flush_events
  (struct vxed_input* input);

END_DECLS

#endif /* INPUT_H */

