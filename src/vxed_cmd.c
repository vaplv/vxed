/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* wordexp support */

#include "vxed.h"
#include "vxed_c.h"
#include "vxed_cmd_c.h"
#include "vxed_conf.h"
#include "vxed_input.h"
#include "vxed_scene.h"
#include "vxed_view.h"
#include "vxed_window.h"

#include <rsys/cstr.h>
#include <rsys/image.h>
#include <rsys/logger.h>
#include <rsys/stretchy_array.h>

#include <unistd.h>
#include <wordexp.h>
#include <readline/readline.h>

#define CMD_DECLARE__(Prefix, Name, Doc)                                       \
  VXED_CMD_DECLARE                                                             \
    (CONCAT(vxed_cmd, CONCAT(Prefix, Name)), STR(Name), Doc,                   \
     CONCAT(vxed_cmd_func, CONCAT(Prefix, Name)), NULL)

/* Main commands */
#define VXED_CMD(Name, Doc)                                                    \
  CMD_DECLARE__(_, Name, Doc);                                                 \
  CMD_DECLARE__(_help_, Name, Doc);
#include "vxed_cmd_decl.h"

/* Show commands */
#define VXED_CMD(Name, Doc)                                                    \
  CMD_DECLARE__(_show_, Name, Doc);                                            \
  CMD_DECLARE__(_help_show_, Name, Doc);
#include "vxed_cmd_show_decl.h"

/* CVar commands */
#define VXED_CMD(Name, Doc)                                                    \
  CMD_DECLARE__(_set_, Name, Doc);                                             \
  CMD_DECLARE__(_show_, Name, Doc);                                            \
  CMD_DECLARE__(_help_set_, Name, Doc);                                        \
  CMD_DECLARE__(_help_show_, Name, Doc);
#include "vxed_cvar_decl.h"

#undef CMD_DECLARE__

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE void
view_update
  (struct vxed_view* view,
   const float rot_speed,
   const float trans_speed,
   const struct vxed_events* e)
{
  float vec[3];
  ASSERT(view && e);

  /* Translation */
  vec[0] = (float)(e->keys[VXED_KEY_MOVE_RIGHT] - e->keys[VXED_KEY_MOVE_LEFT]);
  vec[1] = (float)(e->keys[VXED_KEY_MOVE_UP] - e->keys[VXED_KEY_MOVE_DOWN]);
  vec[2] = (float)(e->keys[VXED_KEY_MOVE_FORWARD] - e->keys[VXED_KEY_MOVE_BACKWARD]);
  f3_mulf(vec, vec, trans_speed * 30.f);
  if(e->buttons & VXED_BUTTON_FLAG(VXED_BUTTON_MOVE)) {
    vec[0] -= (float)e->mouse_dx * trans_speed;
    vec[1] += (float)e->mouse_dy * trans_speed;
  }
  vxed_view_translate(view, vec);

  /* Rotation */
  vec[0] = (float)(e->keys[VXED_KEY_LOOK_UP] - e->keys[VXED_KEY_LOOK_DOWN]);
  vec[1] = (float)(e->keys[VXED_KEY_LOOK_RIGHT] - e->keys[VXED_KEY_LOOK_LEFT]);
  vec[2] = 0.f;
  f3_mulf(vec, vec, rot_speed * 30.f);
  if(e->buttons & VXED_BUTTON_FLAG(VXED_BUTTON_LOOK)) {
    vec[0] -= (float)e->mouse_dy * rot_speed;
    vec[1] += (float)e->mouse_dx * rot_speed;
  }
  vxed_view_rotate(view, vec);
}

static int
cmp_cmd(const void* a, const void* b)
{
  struct vxed_cmd* op0 = *(struct vxed_cmd**)a;
  struct vxed_cmd* op1 = *(struct vxed_cmd**)b;
  return strcmp(op0->name, op1->name);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
vxed_cmd_init
  (struct vxed_cmd* cmd,
   const char* name,
   const char* doc,
   res_T (*func)(struct vxed* vxed, char* arg, void* ctx),
   void* context)
{
  if(!cmd || !name || !doc || !func)
    return RES_BAD_ARG;
  cmd->name = name;
  cmd->doc = doc;
  cmd->func = func;
  cmd->context = context;
  list_init(&cmd->sub_cmds);
  list_init(&cmd->node);
  return RES_OK;
}

res_T
vxed_cmd_register(struct list_node* cmds_list, struct vxed_cmd* cmd)
{
  if(!cmds_list || !cmd) return RES_BAD_ARG;
#ifndef NDEBUG
  { /* Ensure that no other registered command have the same command name */
    struct list_node* node;
    LIST_FOR_EACH(node, cmds_list) {
      struct vxed_cmd* cmd_tmp = CONTAINER_OF(node, struct vxed_cmd, node);
      ASSERT(strcmp(cmd->name, cmd_tmp->name) != 0);
    }
  }
#endif
  list_move(&cmd->node, cmds_list);
  return RES_OK;
}

res_T
vxed_cmd_unregister(struct vxed_cmd* cmd)
{
  if(!cmd) return RES_BAD_ARG;
  list_del(&cmd->node);
  return RES_OK;
}

res_T
vxed_cmd_print
  (struct vxed* vxed,
   const struct list_node* cmds,
   const size_t iline)
{
  size_t iline_scr;
  struct vxed_cmd** sorted_cmds = NULL;
  const struct list_node* node;
  size_t icmd, ncmds;
  int width, height;

  if(!vxed || !cmds)
    return RES_BAD_ARG;

  /* Sort the commands with respect to their names */
  LIST_FOR_EACH(node, cmds) {
    struct vxed_cmd* cmd = CONTAINER_OF(node, struct vxed_cmd, node);
    sa_push(sorted_cmds, cmd);
  }
  ncmds = sa_size(sorted_cmds);
  qsort(sorted_cmds, ncmds, sizeof(struct vxed_cmd*), cmp_cmd);

  /* Printed the sorted list of command names */
  rl_get_screen_size(&height, &width);
  iline_scr = iline;
  for(icmd=0; icmd < ncmds; ) {
    if(iline_scr < (size_t)(height-1)) {
      logger_print(&vxed->logger, LOG_OUTPUT, "%s -- %s\n",
         sorted_cmds[icmd]->name, sorted_cmds[icmd]->doc);
      ++icmd;
      ++iline_scr;
    } else if(vxed_prompt_continue(vxed)) {
      iline_scr = 0;
    } else {
      break;
    }
  }
  sa_release(sorted_cmds);
  return RES_OK;
}

res_T
vxed_cmd_exec(struct vxed* vxed, const struct list_node* cmds, char* cmd_string)
{
  char* cmd_name;
  const struct list_node* node;

  if(!vxed || !cmds || !cmd_string)
    return RES_BAD_ARG;

  cmd_name = strtok(cmd_string, " \t");
  ASSERT(cmd_name);
  LIST_FOR_EACH(node, cmds) {
    struct vxed_cmd* cmd = CONTAINER_OF(node, struct vxed_cmd, node);
    if(!strcmp(cmd_name, cmd->name)) {
      return cmd->func(vxed, strtok(NULL, "\0"), cmd->context);
    }
  }
  logger_print(&vxed->logger, LOG_ERROR,
    "%s: command not found. Try \"help\"\n", cmd_name);
  return RES_BAD_ARG;
}

/*******************************************************************************
 * Command functions functions
 ******************************************************************************/
res_T
vxed_cmd_func_clear(struct vxed* ed, char* opt, void* ctx)
{
  int val;
  ASSERT(ed);
  (void)opt, (void)ctx;

  logger_print(&ed->logger, LOG_OUTPUT, "Remove all instances from the scene? ");
  val = getc(stdin);
  if(val == 'y' || val == 'Y')
    vxed_scene_clear(ed->scene);
  if(val != '\n'|| val != '\r') while(getc(stdin) != '\n'); /* Flush stdin */
  return RES_OK;
}

res_T
vxed_cmd_func_load(struct vxed* ed, char* options, void* ctx)
{
  wordexp_t wexp;
  char* str = NULL;
  unsigned long definition = ed->definition;
  enum vxl_mem_location mem_location = ed->mem_location;
  int opt;
  int err;
  res_T res;
  ASSERT(ed);
  (void)ctx;

  if(!options || !*options) {
    logger_print(&ed->logger, LOG_ERROR, "Argument required\n");
    return RES_BAD_ARG;
  }

  /* Dirtry trick. TODO All the commands should have the argc, argv parameters
   * rather than performing argument expension and split in each command
   * function. */
  str = sa_add(str, strlen("load ") + strlen(options)+1);
  strcpy(str, "load ");
  strcat(str, options);
  err = wordexp(str, &wexp, 0);
  if(err) {
    logger_print(&ed->logger, LOG_ERROR,
      "Error expending string '%s'\n", options);
    return RES_BAD_ARG;
  }

  optind = 1;
  while((opt = getopt((int)wexp.we_wordc, wexp.we_wordv, "m:d:")) != -1) {
    switch(opt) {
      case 'd': res = cstr_to_ulong(optarg, &definition); break;
      case 'm':
        if(!strcmp(optarg, "incore")) {
          mem_location = VXL_MEM_INCORE;
        } else if(!strcmp(optarg, "oocore")) {
          mem_location = VXL_MEM_OOCORE;
        } else {
          res = RES_BAD_ARG;
        }
        break;
      default: res = RES_BAD_ARG; break;
    }
    if(res != RES_OK) {
      if(optarg) {
        logger_print(&ed->logger, LOG_ERROR,
          "load: invalid option argument '%s' -- '%c'\n",
          optarg, opt);
      } else {
        logger_print(&ed->logger, LOG_ERROR,
          "load: invalid option -- '%c'\n",
          opt);
      }
      goto error;
    }
  }

  if(optind == (int)wexp.we_wordc) {
    logger_print(&ed->logger, LOG_ERROR, "load: missing filename.\n");
    res = RES_BAD_ARG;
    goto error;
  }

  res = vxed_scene_load
    (ed->scene, (size_t)definition, mem_location, wexp.we_wordv[optind]);
  if(res != RES_OK) {
    logger_print(&ed->logger, LOG_ERROR,
      "Couldn't load `%s'\n", wexp.we_wordv[optind]);
    return res;
  }

exit:
  if(str) sa_release(str);
  wordfree(&wexp);
  return res;
error:
  goto exit;
}

res_T
vxed_cmd_func_next(struct vxed* ed, char* opt, void* ctx)
{
  (void)opt, (void)ctx;
  return vxed_draw(ed);
}

res_T
vxed_cmd_func_quit(struct vxed* ed, char* opt, void* ctx)
{
  (void)opt, (void)ctx;
  ASSERT(ed);
  ed->state = VXED_EXIT;
  return RES_OK;
}

res_T
vxed_cmd_func_run(struct vxed* ed, char* opt, void* ctx)
{
  struct vxed_events events;
  res_T res = RES_OK;
  (void)opt, (void)ctx;
  VXED_EVENTS_CLEAR(&events);

  VXED(input_flush_events(ed->input));
  for(;;) {
    int buttons;

    res = vxed_draw(ed);
    if(res != RES_OK) break;

    buttons = events.buttons;
    VXED_EVENTS_CLEAR(&events);
    events.buttons = buttons;
    VXED(input_poll_events(ed->input, &events));

    if(events.keys[VXED_KEY_STOP] != 0)
      break;

    view_update(ed->view, ed->camera_speed_rotation,
      ed->camera_speed_translation, &events);
  }
  return res;
}

res_T
vxed_cmd_func_screenshot(struct vxed* ed, char* opt, void* ctx)
{
  wordexp_t wexp;
  struct image img;
  struct SDL_Surface* surface = NULL;
  struct SDL_PixelFormat* pixfmt = NULL;
  const size_t img_Bpp = 3;
  size_t img_pitch;
  unsigned ix, iy;
  res_T res;
  int err;
  (void)ctx;

  if(!opt || !*opt) {
    logger_print(&ed->logger, LOG_ERROR, "Argument required\n");
    return RES_BAD_ARG;
  }
  err = wordexp(opt, &wexp, 0);
  if(err) {
    logger_print(&ed->logger, LOG_ERROR,
      "Error parsing the destination filename\n");
    return RES_BAD_ARG;
  }

  surface = vxed_window_get_SDL_surface(ed->win);
  if(SDL_LockSurface(surface)) {
    logger_print(&ed->logger, LOG_ERROR,
      "Couldn't lock the screen surface: %s\n", SDL_GetError());
    return RES_UNKNOWN_ERR;
  }

  image_init(ed->allocator, &img);

  pixfmt = surface->format;
  img_pitch = (size_t)surface->w * img_Bpp;
  res = image_setup(&img, (size_t)surface->w, (size_t)surface->h, img_pitch,
    IMAGE_RGB8, NULL);
  if(res != RES_OK) {
    logger_print(&ed->logger, LOG_ERROR,
      "Could not setup the screenshot image.\n");
    goto error;
  }

  FOR_EACH(iy, 0, (size_t)surface->h) {
    const char* row_src =  (const char*)surface->pixels + iy * surface->pitch;
    char* row_dst = img.pixels + iy * img_pitch;
    FOR_EACH(ix, 0, (size_t)surface->w) {
      const char* pix_src = row_src + ix * pixfmt->BytesPerPixel;
      unsigned char* pix_dst = (unsigned char*)(row_dst + ix * img_Bpp);
      SDL_GetRGB(*(uint32_t*)pix_src, pixfmt, pix_dst+0, pix_dst+1, pix_dst+2);
    }
  }
  res = image_write_ppm(&img, 0, wexp.we_wordv[0]);
  if(res != RES_OK) {
    logger_print(&ed->logger, LOG_ERROR,
      "Couldn't write the image `%s'\n", wexp.we_wordv[0]);
    res = RES_IO_ERR;
    goto error;
  }

exit:
  wordfree(&wexp);
  SDL_UnlockSurface(surface);
  image_release(&img);
  return res;
error:
  goto exit;
}

res_T
vxed_cmd_func_source(struct vxed* ed, char* opt, void* ctx)
{
  wordexp_t wexp;
  int err;
  (void)ctx;

  if(!opt || !*opt) {
    logger_print(&ed->logger, LOG_ERROR, "Argument required\n");
    return RES_BAD_ARG;
  }
  err = wordexp(opt, &wexp, 0);
  if(err) {
    logger_print(&ed->logger, LOG_ERROR,
      "Error parsing the configuration filename\n");
    return RES_BAD_ARG;
  }
  return vxed_conf_load(ed, wexp.we_wordv[0]);
}


