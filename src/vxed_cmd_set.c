/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxed.h"
#include "vxed_cmd_c.h"
#include "vxed_parse.h"
#include "vxed_scene.h"
#include "vxed_view.h"
#include "vxed_window.h"

#include <aw.h>
#include <vx/vxaw.h>
#include <vx/vxl.h>

#include <rsys/float3.h>
#include <rsys/logger.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE res_T
set_float3(struct vxed* ed, char* str, float dst[3])
{
  float tmp[3];
  res_T res;
  ASSERT(dst);
  if(!str) {
    logger_print(&ed->logger, LOG_ERROR, "Argument required\n");
    return RES_BAD_ARG;
  }
  res = vxed_parse_float3(str, tmp);
  if(res != RES_OK) {
    logger_print(&ed->logger, LOG_ERROR,
      "Couldn't parse the float3 `%s'\n", str);
    return res;
  }
  f3_set(dst, tmp);
  return RES_OK;
}

static INLINE res_T
set_float(struct vxed* ed, char* str, float* dst)
{
  float f;
  res_T res;
  ASSERT(ed && dst);
  if(!str) {
    logger_print(&ed->logger, LOG_ERROR, "Argument required\n");
    return RES_BAD_ARG;
  }
  res = vxed_str_to_float(str, &f);
  if(res != RES_OK) {
    logger_print(&ed->logger, LOG_ERROR,
      "Couldn't parse the float `%s'\n", str);
    return res;
  }
  *dst = f;
  return RES_OK;
}

static INLINE res_T
set_float_positive(struct vxed* ed, char* str, float* dst)
{
  float f;
  res_T res;
  ASSERT(dst);
  res = set_float(ed, str, &f);
  if(res != RES_OK)
    return res;
  if(f < 0.f) {
    logger_print(&ed->logger, LOG_ERROR,
      "Invalid negative argument `%.2f'\n", f);
    return RES_BAD_ARG;
  }
  *dst = f;
  return RES_OK;
}

static INLINE res_T
set_long(struct vxed* ed, char* str, long* dst)
{
  long i;
  res_T res;
  ASSERT(ed);

  if(!str) {
    logger_print(&ed->logger, LOG_ERROR, "Argument required\n");
    return RES_BAD_ARG;
  }
  res = vxed_str_to_long(str, &i);
  if(res != RES_OK) {
    logger_print(&ed->logger, LOG_ERROR,
      "Couldn't parse the integer `%s'\n", str);
    return res;
  }
  *dst = i;
  return RES_OK;
}

static INLINE res_T
set_cvar_opt(struct vxed* ed, const enum option cvar, char* val)
{
  long i;
  res_T res;
  ASSERT(ed && (unsigned)cvar < OPT_COUNT__);
  if(RES_OK == (res = set_long(ed, strtok(val, " \t"), &i)))
    ed->options[cvar] = (int)i;
  return res;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
vxed_cmd_func_set(struct vxed* ed, char* opt, void* ctx)
{
  (void)ctx;
  if(!opt || !*opt) {
    logger_print(&ed->logger, LOG_ERROR, "Argument required\n");
    return RES_BAD_ARG;
  }
  return vxed_cmd_exec(ed, &vxed_cmd_set.sub_cmds, opt);
}

res_T
vxed_cmd_func_set_camera_speed_rotation(struct vxed* ed, char* opt, void* ctx)
{
  (void)ctx;
  ASSERT(ed);
  return set_float_positive(ed, strtok(opt, " \t"), &ed->camera_speed_rotation);
}

res_T
vxed_cmd_func_set_camera_speed_translation(struct vxed* ed, char* opt, void* ctx)
{
  (void)ctx;
  ASSERT(ed);
  return set_float_positive
    (ed, strtok(opt, " \t"), &ed->camera_speed_translation);
}

res_T
vxed_cmd_func_set_field_of_view(struct vxed* ed, char* opt, void* ctx)
{
  float fov;
  char* tk;
  res_T res;
  (void)ctx;
  ASSERT(ed);

  tk = strtok(opt, " \t");
  res = vxed_parse_fov(tk, &fov);
  if(res != RES_OK) {
    logger_print(&ed->logger, LOG_ERROR,
      "Invalid field of view value `%s'\n", tk);
    return res;
  }
  return vxed_view_set_fov(ed->view, fov);
}

res_T
vxed_cmd_func_set_point_of_view(struct vxed* ed, char* opt, void* ctx)
{
  char* pos_str, *tgt_str, *up_str;
  float pos[3], tgt[3], up[3];
  res_T res;
  ASSERT(ed);
  (void)ctx;

  if(!opt) {
    logger_print(&ed->logger, LOG_ERROR, "Argument required\n");
    return RES_BAD_ARG;
  }
  vxed_view_get_look_at(ed->view, pos, tgt, up);
  pos_str = strtok(opt, " \t");
  tgt_str = strtok(NULL, " \t");
  up_str = strtok(NULL, " \t");
  if(pos_str && RES_OK != (res=set_float3(ed, pos_str, pos))) return res;
  if(tgt_str && RES_OK != (res=set_float3(ed, tgt_str, tgt))) return res;
  if(up_str  && RES_OK != (res=set_float3(ed, up_str,  up)))  return res;
  return vxed_view_set_look_at(ed->view, pos, tgt, up);
}

res_T
vxed_cmd_func_set_print_load_perf(struct vxed* ed, char* opt, void* ctx)
{
  (void)ctx;
  return set_cvar_opt(ed, OPT_PRINT_LOAD_PERF, opt);
}

res_T
vxed_cmd_func_set_print_render_perf(struct vxed* ed, char* opt, void* ctx)
{
  (void)ctx;
  return set_cvar_opt(ed, OPT_PRINT_RENDER_PERF, opt);
}

res_T
vxed_cmd_func_set_video_mode(struct vxed* ed, char* opt, void* ctx)
{
  res_T res = RES_OK;
  unsigned vmode[3];
  ASSERT(ed);
  (void)ctx;

  if(!opt) {
    logger_print(&ed->logger, LOG_ERROR, "Argument required\n");
    return RES_BAD_ARG;
  }

  res = vxed_parse_video_mode(opt, vmode);
  if(res != RES_OK) {
    logger_print(&ed->logger, LOG_ERROR,
      "Invalid video mode formatting ``%s''\n", opt);
    return res;
  }
  return vxed_window_set_video_mode(ed->win, vmode);
}

res_T
vxed_cmd_func_set_vxl_definition(struct vxed* ed, char* opt, void* ctx)
{
  long i;
  res_T res = RES_OK;
  (void)ctx;

  if(RES_OK != (res = set_long(ed, strtok(opt, " \t"), &i))) return res;
  if(i < 0) {
    logger_print(&ed->logger, LOG_ERROR, "The vxl_definition must be positive\n");
    return RES_BAD_ARG;
  }
  ed->definition = (size_t)i;
  return RES_OK;
}

res_T
vxed_cmd_func_set_vxl_LOD_min(struct vxed* ed, char* opt, void* ctx)
{
  long i;
  res_T res;
  STATIC_ASSERT(sizeof(int) <= sizeof(uint32_t), Unexpected_type_size);
  ASSERT(ed);
  (void)ctx;

  if(RES_OK != (res = set_long(ed, strtok(opt, " \t"), &i))) return res;
  if(i < 0) {
    logger_print(&ed->logger, LOG_ERROR, "The minimum LOD must be positive\n");
    return RES_BAD_ARG;
  }
  ed->vxr_draw_state.rt_state.lod_min = (uint32_t)i;
  ed->vxr_draw_state.rt_state.lod_max = MMAX
    (ed->vxr_draw_state.rt_state.lod_max,
     ed->vxr_draw_state.rt_state.lod_min);
  return RES_OK;
}

res_T
vxed_cmd_func_set_vxl_LOD_max(struct vxed* ed, char* opt, void* ctx)
{
  long i;
  res_T res;
  STATIC_ASSERT(sizeof(int) <= sizeof(uint32_t), Unexpected_type_size);
  ASSERT(ed);
  (void)ctx;

  if(RES_OK != (res = set_long(ed, strtok(opt, " \t"), &i))) return res;
  if(i < 0) {
    logger_print(&ed->logger, LOG_ERROR, "The maximum LOD must be positive\n");
    return RES_BAD_ARG;
  }
  ed->vxr_draw_state.rt_state.lod_max = (uint32_t)i;
  ed->vxr_draw_state.rt_state.lod_min = MMIN
    (ed->vxr_draw_state.rt_state.lod_min,
     ed->vxr_draw_state.rt_state.lod_max);
  return RES_OK;
}

res_T
vxed_cmd_func_set_vxl_rt_simd(struct vxed* ed, char* opt, void* ctx)
{
  long i;
  res_T res;
  ASSERT(ed);
  (void)ctx;

  if(RES_OK != (res = set_long(ed, strtok(opt, " \t"), &i))) return res;
  if(i != 0) {
    ed->vxr_draw_state.rt_state.simd = 1;
  } else {
    ed->vxr_draw_state.rt_state.simd = 0;
  }
  return RES_OK;
}

res_T
vxed_cmd_func_set_vxr_beam_prepass(struct vxed* ed, char* opt, void* ctx)
{
  long i;
  res_T res;
  ASSERT(ed);
  (void)ctx;

  if(RES_OK != (res = set_long(ed, strtok(opt, " \t"), &i))) return res;
  if(i!=0) {
    ed->vxr_draw_state.mask |= VXR_DRAW_BEAM_PREPASS;
  } else {
    ed->vxr_draw_state.mask &= ~VXR_DRAW_BEAM_PREPASS;
  }
  return RES_OK;
}

res_T
vxed_cmd_func_set_vxr_render_type(struct vxed* ed, char* opt, void* ctx)
{
  ASSERT(ed);
  (void)ctx;

  if(!opt || !(opt = strtok(opt, " \t"))) {
    logger_print(&ed->logger, LOG_ERROR, "Argument required\n");
    return RES_BAD_ARG;
  }

  if(!strcmp(opt, "legacy")) {
    ed->vxr_draw_state.render_type = VXR_RENDER_LEGACY;
  } else if(!strcmp(opt, "cubic")) {
    ed->vxr_draw_state.render_type = VXR_RENDER_CUBIC;
  } else if(!strcmp(opt, "normal")) {
    ed->vxr_draw_state.render_type = VXR_RENDER_NORMAL;
  } else if(!strcmp(opt, "color")) {
    ed->vxr_draw_state.render_type = VXR_RENDER_COLOR;
  } else {
    logger_print(&ed->logger, LOG_ERROR, "Invalid render type `%s'\n", opt);
    return RES_BAD_ARG;
  }
  return RES_OK;
}

