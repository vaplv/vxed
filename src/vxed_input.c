/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxed_input.h"

#include <rsys/hash_table.h>
#include <rsys/ref_count.h>

#include <limits.h>

static FINLINE char
vxed_input_key_desc_eq
  (const struct vxed_input_key_desc* a,
   const struct vxed_input_key_desc* b)
{
  ASSERT(a && b);
  return a->key == b->key && (a->mod & b->mod) == a->mod;
}

/* Declare the key_binding hash table */
#define HTABLE_NAME key_binding
#define HTABLE_KEY struct vxed_input_key_desc
#define HTABLE_KEY_FUNCTOR_EQ vxed_input_key_desc_eq
#define HTABLE_DATA enum vxed_input_key
#include <rsys/hash_table.h>

/* Declare the button_binding hash table */
#define HTABLE_NAME button_binding
#define HTABLE_KEY uint8_t
#define HTABLE_DATA enum vxed_input_button
#include <rsys/hash_table.h>

struct vxed_input {
  struct htable_key_binding key_binding;
  struct htable_button_binding button_binding;
  ref_T ref;
  struct vxed* vxed;
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE void
on_key_event
  (struct vxed_input* in,
   struct vxed_events* events,
   const SDL_Event* evt)
{
  enum vxed_input_key* ikey;
  struct vxed_input_key_desc desc;
  ASSERT(in && events && evt);
  ASSERT(evt->type == SDL_KEYDOWN || evt->type == SDL_KEYUP);

  desc.key = evt->key.keysym.sym;
  desc.mod = evt->key.keysym.mod & (KMOD_CTRL|KMOD_SHIFT|KMOD_ALT|KMOD_META);

  ikey = htable_key_binding_find(&in->key_binding, &desc);
  if(ikey) {
    ASSERT(*ikey < VXED_KEYS_COUNT__);
    events->keys[*ikey] += events->keys[*ikey] < INT_MAX;
  }
}

static INLINE void
on_mouse_motion_event
  (struct vxed_input* in,
   struct vxed_events* events,
   const SDL_Event* evt)
{
  ASSERT(in && events && evt && evt->type == SDL_MOUSEMOTION);
  events->mouse_dx += evt->motion.xrel;
  events->mouse_dy += evt->motion.yrel;
  (void)in;
}

static FINLINE void
on_mouse_button_event
  (struct vxed_input* in,
   struct vxed_events* events,
   const SDL_Event* evt)
{
  enum vxed_input_button* ibutton;
  ASSERT(in && events && evt);
  ASSERT(evt->type == SDL_MOUSEBUTTONDOWN || evt->type == SDL_MOUSEBUTTONUP);

  switch(evt->button.button) {
    case SDL_BUTTON_WHEELUP:
      events->keys[VXED_KEY_MOVE_FORWARD] +=
        events->keys[VXED_KEY_MOVE_FORWARD] < INT_MAX;
      break;
    case SDL_BUTTON_WHEELDOWN:
      events->keys[VXED_KEY_MOVE_BACKWARD] +=
        events->keys[VXED_KEY_MOVE_FORWARD] < INT_MAX;
      break;
    default:
      ibutton = htable_button_binding_find
        (&in->button_binding, &evt->button.button);
      if(ibutton) {
        ASSERT(*ibutton < VXED_BUTTONS_COUNT__);
        if(evt->button.state == SDL_PRESSED) {
          events->buttons = VXED_BUTTON_FLAG(*ibutton);
        } else {
          events->buttons = events->buttons & ~VXED_BUTTON_FLAG(*ibutton);
        }
      }
      break;
  }
}

static void
vxed_input_release(ref_T* ref)
{
  struct vxed_input* input;
  ASSERT(ref);
  input = CONTAINER_OF(ref, struct vxed_input, ref);
  htable_key_binding_release(&input->key_binding);
  htable_button_binding_release(&input->button_binding);
  MEM_RM(input->vxed->allocator, input);
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
VXED_DEFINE_REF_FUNCS(vxed_input)

res_T
vxed_input_create
  (struct vxed* ed,
   const struct vxed_input_key_desc keys[VXED_KEYS_COUNT__],
   const uint8_t buttons[VXED_BUTTONS_COUNT__],
   struct vxed_input** out_input)
{
  struct vxed_input* input = NULL;
  enum vxed_input_key ikey;
  enum vxed_input_button ibutton;
  res_T res;

  if(!ed || !keys || !buttons || !out_input) {
    res = RES_BAD_ARG;
    goto error;
  }

  input = MEM_CALLOC(ed->allocator, 1, sizeof(struct vxed_input));
  if(!input) {
    logger_print(&ed->logger, LOG_ERROR,
      "Not enough memory: couldn't allocate the VxEd input\n");
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&input->ref);
  input->vxed = ed;
  htable_key_binding_init(ed->allocator, &input->key_binding);
  htable_button_binding_init(ed->allocator, &input->button_binding);

  FOR_EACH(ikey, 0, VXED_KEYS_COUNT__) {
    res = htable_key_binding_set(&input->key_binding, keys + ikey, &ikey);
    if(res != RES_OK) goto error;
  }

  FOR_EACH(ibutton, 0, VXED_BUTTONS_COUNT__) {
    res = htable_button_binding_set
      (&input->button_binding, buttons + ibutton, &ibutton);
    if(res != RES_OK) goto error;
  }

  if(SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY, SDL_DEFAULT_REPEAT_INTERVAL))
    goto error;

exit:
  if(out_input) *out_input = input;
  return res;
error:
  if(input) {
    vxed_input_ref_put(input);
    input = NULL;
  }
  goto exit;
}

res_T
vxed_input_poll_events(struct vxed_input* input, struct vxed_events* events)
{
  SDL_Event evt;

  if(!input || !events)
    return RES_BAD_ARG;

  while(SDL_PollEvent(&evt)) {
    switch(evt.type) {
      case SDL_KEYUP:
      case SDL_KEYDOWN:
        on_key_event(input, events, &evt);
        break;
      case SDL_MOUSEMOTION:
        on_mouse_motion_event(input, events, &evt);
        break;
      case SDL_MOUSEBUTTONUP:
      case SDL_MOUSEBUTTONDOWN:
        on_mouse_button_event(input, events, &evt);
        break;
      default: /* Silently discard untracked events */ break;
    }
  }
  return RES_OK;
}

res_T
vxed_input_flush_events(struct vxed_input* input)
{
  SDL_Event evt;
  if(!input) return RES_BAD_ARG;
  while(SDL_PollEvent(&evt));
  return RES_OK;
}

