/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxed.h"
#include "vxed_window.h"

#include <rsys/logger.h>
#include <rsys/ref_count.h>
#include <vx/vxr.h>

struct vxed_window {
  struct vxr_framebuffer* vxr_fbuf;
  SDL_Surface* surface;
  unsigned width, height, bpp;

  signal_T signals[VXED_WINDOW_SIGS_COUNT__];

  ref_T ref;
  struct vxed* ed;
};

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static res_T
surface_write
  (void* ctx,
   const unsigned origin[2],
   const unsigned size[2],
   const enum vxr_pixel_format format,
   const void* pixels)
{
  struct vxed_window* win = ctx;
  struct SDL_PixelFormat* pixfmt;
  struct SDL_Surface* surface;
  size_t bpp_src;
  size_t pitch_src;
  unsigned ix, iy;
  ASSERT(ctx && origin && size);

  surface = win->surface;
  if(SDL_LockSurface(surface)) {
    logger_print(&win->ed->logger, LOG_ERROR,
      "Couldn't lock screen surface: %s\n", SDL_GetError());
    return RES_UNKNOWN_ERR;
  }
  if(format != VXR_UBYTE_RGBA) {
    logger_print(&win->ed->logger, LOG_ERROR,
      "Only UBYTE_RGBA framebuffer are supported\n");
    return RES_BAD_ARG;
  }

  bpp_src = VXR_SIZEOF_PIXEL_FORMAT(format);
  pitch_src = bpp_src * size[0];
  pixfmt = surface->format;

  FOR_EACH(iy, 0, size[1]) {
    char* row_dst = (char*)surface->pixels + (iy + origin[1]) * surface->pitch;
    char* row_src = (char*)pixels + iy * pitch_src;

    FOR_EACH(ix, 0, size[0]) {
      char* pixdst = row_dst + (ix + origin[0]) * pixfmt->BytesPerPixel;
      uint8_t* pixsrc = (uint8_t*)(row_src + ix * bpp_src);
      uint32_t rgba;
      rgba = SDL_MapRGBA(pixfmt, pixsrc[0], pixsrc[1], pixsrc[2], pixsrc[3]);

      switch(pixfmt->BytesPerPixel) {
        case 1: *(uint8_t*)pixdst = (uint8_t)rgba; break;
        case 2: *(uint16_t*)pixdst = (uint16_t)rgba; break;
        case 3:
          if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
            ((uint8_t*)pixdst)[0] = (rgba >> 16) & 0xFF;
            ((uint8_t*)pixdst)[1] = (rgba >> 8)  & 0xFF;
            ((uint8_t*)pixdst)[2] = (rgba >> 0)  & 0xff;
          } else {
            ((uint8_t*)pixdst)[2] = (rgba >> 16) & 0xFF;
            ((uint8_t*)pixdst)[1] = (rgba >> 8)  & 0xFF;
            ((uint8_t*)pixdst)[0] = (rgba >> 0)  & 0xff;
          }
          break;
        case 4: *(uint32_t*)pixdst = rgba; break;
        default: FATAL("Unreachable code\n"); break;
      }
    }
  }
  SDL_UnlockSurface(surface);
  return RES_OK;
}

static res_T
create_vxr_framebuffer(struct vxed_window* win)
{
  struct vxr_framebuffer* fbuf;
  res_T res;
  ASSERT(win);
  res = vxr_framebuffer_create(win->ed->vxr, &fbuf);
  if(res != RES_OK) {
    logger_print(&win->ed->logger, LOG_ERROR,
      "Could not create the VoXel renderer framebuffer\n");
    return res;
  }
  if(win->vxr_fbuf) VXR(framebuffer_ref_put(win->vxr_fbuf));
  win->vxr_fbuf = fbuf;
  return RES_OK;
}

static void
vxed_window_release(ref_T* ref)
{
  struct vxed_window* win = CONTAINER_OF(ref, struct vxed_window, ref);
  ASSERT(ref);
  if(win->vxr_fbuf) VXR(framebuffer_ref_put(win->vxr_fbuf));
  MEM_RM(win->ed->allocator, win);
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
VXED_DEFINE_REF_FUNCS(vxed_window)

res_T
vxed_window_create
  (struct vxed* ed,
   const unsigned vmode[3],
   struct vxed_window** out_win)
{
  struct vxed_window* win;
  int i;
  res_T res = RES_OK;
  ASSERT(ed && out_win);

  win = MEM_CALLOC(ed->allocator, 1, sizeof(struct vxed_window));
  if(!win) {
    logger_print(&ed->logger, LOG_ERROR, "Could not create the window\n");
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&win->ref);
  win->ed = ed;
  win->width = (unsigned)~0;
  win->height = (unsigned)~0;
  win->bpp = (unsigned)~0;
  FOR_EACH(i, 0, VXED_WINDOW_SIGS_COUNT__) SIG_INIT(win->signals + i);

  res = create_vxr_framebuffer(win);
  if(res != RES_OK) goto error;

  res = vxed_window_set_video_mode(win, vmode);
  if(res != RES_OK)
    goto error;

exit:
  *out_win = win;
  return res;
error:
  if(win) {
    vxed_window_ref_put(win);
    win = NULL;
  }
  goto exit;
}

res_T
vxed_window_set_video_mode(struct vxed_window* win, const unsigned vmode[3])
{
  SDL_Surface* surface;
  const uint32_t video_mask = SDL_HWSURFACE|SDL_ANYFORMAT;
  int bpp_adjusted;
  res_T res = RES_OK;
  ASSERT(win && vmode);

  bpp_adjusted = SDL_VideoModeOK
    ((int)vmode[0], (int)vmode[1], (int)vmode[2], video_mask);
  if(!bpp_adjusted) {
    logger_print(&win->ed->logger, LOG_ERROR,
      "Unavailable video mode %ux%u@%ubpp\n", SPLIT3(vmode));
    res = RES_BAD_ARG;
    goto error;
  }

  if(bpp_adjusted != (int)vmode[2]) {
    logger_print(&win->ed->logger, LOG_WARNING,
      "Closest available video mode %u%u@%ubpp\n",
      vmode[0], vmode[1], bpp_adjusted);
  }

  surface = SDL_SetVideoMode
    ((int)vmode[0], (int)vmode[1], bpp_adjusted, video_mask);
  if(!surface) {
    logger_print(&win->ed->logger, LOG_ERROR,
      "Could not set the video mode %ux%u@%ubpp\n",
      vmode[0], vmode[1], bpp_adjusted);
    res = RES_BAD_ARG;
    goto error;
  }
  win->surface = surface;
  win->width = vmode[0];
  win->height = vmode[1];
  win->bpp = (unsigned)bpp_adjusted;

  res = vxr_framebuffer_setup(win->vxr_fbuf, vmode, surface_write, win);
  if(res != RES_OK) {
    logger_print(&win->ed->logger, LOG_ERROR,
      "Could not setup the VoXel Renderer framebufffer to %ux%u\n",
      vmode[0], vmode[1]);
    goto error;
  }
  SIG_BROADCAST(win->signals + VXED_WINDOW_SIG_UPDATE_MODE,
    vxed_window_cb_T, ARG1(win));
exit:
  return res;
error:
  goto exit;
}

void
vxed_window_get_video_mode(const struct vxed_window* win, unsigned vmode[3])
{
  ASSERT(win && vmode);
  vmode[0] = win->width;
  vmode[1] = win->height;
  vmode[2] = win->bpp;
}

SDL_Surface*
vxed_window_get_SDL_surface(struct vxed_window* win)
{
  ASSERT(win && win->surface);
  return win->surface;
}

void
vxed_window_swap(struct vxed_window* win)
{
  ASSERT(win && win->surface);
  SDL_Flip(win->surface);
}

res_T
vxed_window_get_vxr_framebuffer(struct vxed_window* win, struct vxr_framebuffer** fbuf)
{
  ASSERT(win && fbuf);
  *fbuf = win->vxr_fbuf;
  return RES_OK;
}

float
vxed_window_get_pixel_ratio(struct vxed_window* win)
{
  ASSERT(win);
  return (float)win->width/(float)win->height;
}

void
vxed_window_listen
  (struct vxed_window* win,
   const enum vxed_window_sig sig,
   vxed_window_cb_T* cb)
{
  ASSERT(win && (unsigned)sig < VXED_WINDOW_SIGS_COUNT__ && cb);
  SIG_CONNECT_CLBK(win->signals + sig, cb);
}

