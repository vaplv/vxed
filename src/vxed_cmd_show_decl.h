/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXED_CMD
  #define VXED_CMD(Name, Doc)
#endif

VXED_CMD(copying, "Conditions for redistributing copies of vxed.")
VXED_CMD(cvars, "Value of the config variables.")
VXED_CMD(scene_info, "Informations on the current scene.")
VXED_CMD(warranty, "Various kinds of warranty that you do not have.")

#undef VXED_CMD

