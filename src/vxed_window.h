/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXED_WINDOW_H
#define VXED_WINDOW_H

#include "vxed.h"

#include <rsys/signal.h>
#include <SDL.h>

struct vxed_window;

CLBK(vxed_window_cb_T, ARG1(struct vxed_window*));

enum vxed_window_sig {
  VXED_WINDOW_SIG_UPDATE_MODE,
  VXED_WINDOW_SIGS_COUNT__
};

BEGIN_DECLS

VXED_API res_T
vxed_window_create
  (struct vxed* ed,
   const unsigned video_mode[3], /* width, height, bits per pixel */
   struct vxed_window** win);

VXED_API void
vxed_window_ref_get
  (struct vxed_window* win);

VXED_API void
vxed_window_ref_put
  (struct vxed_window* win);

VXED_API res_T
vxed_window_set_video_mode
  (struct vxed_window* win,
   const unsigned video_mode[3]); /* width, height, bits per pixel */

VXED_API void
vxed_window_get_video_mode
  (const struct vxed_window* win,
   unsigned video_mode[3]); /* width, height, bits per pixel */

VXED_API SDL_Surface*
vxed_window_get_SDL_surface
  (struct vxed_window* win);

VXED_API void
vxed_window_swap
  (struct vxed_window* win);

VXED_API res_T
vxed_window_get_vxr_framebuffer
  (struct vxed_window* win,
   struct vxr_framebuffer** fbuf);

VXED_API float /* width / height */
vxed_window_get_pixel_ratio
  (struct vxed_window* win);

VXED_API void
vxed_window_listen
  (struct vxed_window* win,
   const enum vxed_window_sig sig,
   vxed_window_cb_T* cb);

#endif /* VIDEO_H */

