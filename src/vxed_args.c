/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 2 /* getopt support */

#include "vxed_args.h"
#include "vxed_parse.h"

#include <rsys/cstr.h>

#include <string.h>
#include <unistd.h>

/*******************************************************************************
 * Helper function
 ******************************************************************************/
static void
print_options(void)
{
  printf(
"  -c CONF_FILE select a file from which the vxed configuration is read.\n"
"               These setings are priority on command line arguments.\n"
"               By default the %s file is loaded if it exists\n",
    VXED_ARGS_DEFAULT.conf_filename);
  printf(
"  -d DEFINITION\n"
"               set the voxelization definition along the X, Y and Z axis.\n"
"               Default is %lu.\n",
    VXED_ARGS_DEFAULT.definition);
  printf(
"  -f FOV       set the horizontal field of view to FOV degree. Default is %.1f\n",
    MRAD2DEG(VXED_ARGS_DEFAULT.view_fov));
  printf(
"  -h           display this help and exit\n");
  printf(
"  -m <incore|oocore>\n"
"               memory location of the voxels. Default is %s\n",
    VXED_ARGS_DEFAULT.mem_location == VXL_MEM_INCORE ? "incore" : "oocore");
  printf(
"  -n NTHREADS  hint on the number of threads to use. By default the\n"
"               application uses as many threads as available CPU cores\n");
  printf(
"  -p X:Y:Z     set the view position. Default is %.1f:%.1f:%.1f\n",
    SPLIT3(VXED_ARGS_DEFAULT.view_position));
  printf(
"  -t X:Y:Z     set the view target. Default is %.1f:%.1f:%.1f\n",
    SPLIT3(VXED_ARGS_DEFAULT.view_target));
  printf(
"  -u X:Y:Z     set the view up vector. Default is %.1f:%.1f:%.1f\n",
    SPLIT3(VXED_ARGS_DEFAULT.view_up));
  printf(
"  -v           make the command more verbose\n");
  printf(
"  -V WIDTHxHEIGHT@BPP\n"
"               set the window size to WIDTHxHEIGHT with a pixel depth of BPP\n"
"               bits per pixel. Default is %dx%d@%d\n",
    SPLIT3(VXED_ARGS_DEFAULT.video_mode));
}

static res_T
parse_mem_location(char* str, struct vxed_args* args)
{
  res_T res = RES_OK;
  if(!strcmp(str, "incore")) {
    args->mem_location = VXL_MEM_INCORE;
  } else if(!strcmp(str, "oocore")) {
    args->mem_location = VXL_MEM_OOCORE;
  } else {
    res = RES_BAD_ARG;
  }
  return res;
}

static res_T
parse_nthreads(char* str, struct vxed_args* args)
{
  long nthreads;
  res_T res;
  ASSERT(args && str);
  res = vxed_str_to_long(str, &nthreads);
  if(res != RES_OK)
    return res;
  if(nthreads <= 0)
    return RES_BAD_ARG;
  args->nthreads_hint = (unsigned)nthreads;
  return res;
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
vxed_args_parse
  (struct vxed_args* args,
   int* quit,
   const int argc,
   char* argv[])
{
  int opt;
  res_T res = RES_OK;
  ASSERT(args && argc && argv);

  *quit = 0;
  *args = VXED_ARGS_DEFAULT;
  args->conf_filename = NULL;
  while((opt = getopt(argc, argv, "c:d:f:hm:n:p:t:u:vV:")) != -1) {
    switch(opt) {
      case 'c': args->conf_filename = optarg; break;
      case 'd': res = cstr_to_ulong(optarg, &args->definition); break;
      case 'f': res = vxed_parse_fov(optarg, &args->view_fov); break;
      case 'h':
        printf(
          "Usage: %s [OPTION]... [FILE]\n"
          "Launch the VoXel EDitor to visualise and bench the voxelized representation \n"
          "of the submitted FILE. Currently only Alias Wavefront obj FILEs are supported\n\n",
          argv[0]);
        print_options();
        *quit = 1;
        return RES_OK;
      case 'm': res = parse_mem_location(optarg, args); break;
      case 'n': res = parse_nthreads(optarg, args); break;
      case 'p': res = vxed_parse_float3(optarg, args->view_position); break;
      case 't': res = vxed_parse_float3(optarg, args->view_target); break;
      case 'u': res = vxed_parse_float3(optarg, args->view_up); break;
      case 'v': args->verbose = 1; break;
      case 'V': res = vxed_parse_video_mode(optarg, args->video_mode); break;
      default: res = RES_BAD_ARG; break;
    }
    if(res != RES_OK) {
      if(optarg) {
        fprintf(stderr, "%s: invalid option argument '%s' -- '%c'\n",
          argv[0], optarg, opt);
      }
      return res;
    }
  }
  if(!args->conf_filename && access(VXED_ARGS_DEFAULT.conf_filename, F_OK) != -1)
    args->conf_filename = VXED_ARGS_DEFAULT.conf_filename;

  ASSERT(optind >= 0);
  args->model = argv[optind];
  return RES_OK;
}

