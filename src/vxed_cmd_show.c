/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxed.h"
#include "vxed_c.h"
#include "vxed_cmd_c.h"
#include "vxed_copying.h"
#include "vxed_scene.h"
#include "vxed_view.h"
#include "vxed_warranty.h"
#include "vxed_window.h"

#include <vx/vxl.h>

#include <rsys/logger.h>

#include <limits.h>
#include <readline/readline.h>

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
vxed_cmd_func_show(struct vxed* ed, char* opt, void* ctx)
{
  ASSERT(ed);
  (void)ctx;
  if(!opt || !*opt) {
    logger_print(&ed->logger, LOG_ERROR, "Argument required\n");
    return RES_BAD_ARG;
  }
  return vxed_cmd_exec(ed, &vxed_cmd_show.sub_cmds, opt);
}

res_T
vxed_cmd_func_show_copying(struct vxed* ed, char* opt, void* ctx)
{
  (void)opt, (void)ctx;
  return vxed_print_text(ed, g_copying, sizeof(g_copying)/sizeof(const char*));
}

res_T
vxed_cmd_func_show_cvars(struct vxed* ed, char* opt, void* ctx)
{
  size_t iline_scr;
  int width, height;
  ASSERT(ed);
  (void)opt, (void)ctx;

  rl_get_screen_size(&height, &width);
  iline_scr = 0;

  #define VXED_CMD(Name, Doc) {                                                \
    res_T res;                                                                 \
    if(iline_scr >= (size_t)(height - 1)) {                                    \
      if(!vxed_prompt_continue(ed)) {                                          \
        return RES_OK;                                                         \
      } else {                                                                 \
        iline_scr = 0;                                                         \
      }                                                                        \
    }                                                                          \
    res = vxed_cmd_show_ ## Name.func                                          \
      (ed, STR(Name), vxed_cmd_show_##Name.context);                           \
    if(res != RES_OK) return res;                                              \
    ++iline_scr;                                                               \
  }
  #include "vxed_cvar_decl.h"
  return RES_OK;
}

res_T
vxed_cmd_func_show_camera_speed_rotation(struct vxed* ed, char* opt, void* ctx)
{
  ASSERT(ed);
  (void)ctx, (void)opt;
  logger_print(&ed->logger, LOG_OUTPUT,
    "camera_speed_rotation = %f\n", ed->camera_speed_rotation);
  return RES_OK;
}

res_T
vxed_cmd_func_show_camera_speed_translation(struct vxed* ed, char* opt, void* ctx)
{
  ASSERT(ed);
  (void)ctx, (void)opt;
  logger_print(&ed->logger, LOG_OUTPUT,
    "camera_speed_translation = %f\n", ed->camera_speed_translation);
  return RES_OK;
}

res_T
vxed_cmd_func_show_field_of_view(struct vxed* ed, char* opt, void* ctx)
{
  ASSERT(ed);
  (void)ctx, (void)opt;
  logger_print(&ed->logger, LOG_OUTPUT,
    "field_of_view = %.2f\n", (float)MRAD2DEG(vxed_view_get_fov(ed->view)));
  return RES_OK;
}

res_T
vxed_cmd_func_show_point_of_view(struct vxed* ed, char* opt, void* ctx)
{
  float pos[3], tgt[3], up[3];
  ASSERT(ed);
  (void)ctx, (void)opt;
  vxed_view_get_look_at(ed->view, pos, tgt, up);
  logger_print(&ed->logger, LOG_OUTPUT,
    "point_of_view = %.2f:%.2f:%.2f %.2f:%.2f:%.2f %.2f:%.2f:%.2f\n",
    SPLIT3(pos), SPLIT3(tgt), SPLIT3(up));
  return RES_OK;
}

res_T
vxed_cmd_func_show_print_render_perf(struct vxed* ed, char* opt, void* ctx)
{
  ASSERT(ed);
  (void)ctx, (void)opt;
  logger_print(&ed->logger, LOG_OUTPUT,
    "print_render_perf = %d\n", ed->options[OPT_PRINT_RENDER_PERF]);
  return RES_OK;
}

res_T
vxed_cmd_func_show_print_load_perf(struct vxed* ed, char* opt, void* ctx)
{
  ASSERT(ed);
  (void)ctx, (void)opt;
  logger_print(&ed->logger, LOG_OUTPUT,
    "print_load_perf = %d\n", ed->options[OPT_PRINT_LOAD_PERF]);
  return RES_OK;
}

res_T
vxed_cmd_func_show_scene_info(struct vxed* ed, char* opt, void* ctx)
{
  struct vxl_scene_desc desc;
  struct vxl_scene* vxl_scn;
  res_T res;
  ASSERT(ed);
  (void)opt, (void)ctx;

  vxl_scn = vxed_scene_get_vxl_scene(ed->scene);
  res = vxl_scene_get_desc(vxl_scn, &desc);
  if(res != RES_OK) {
    logger_print(&ed->logger, LOG_ERROR,
      "Could not retrieve the scene informations\n");
    return res;
  }
  logger_print(&ed->logger, LOG_OUTPUT,
    "Scene informations:\n"
    "  Number of voxels: %lu\n",
    (unsigned long)desc.voxels_count);

  if(desc.mem_size < 1024*1024) {
    logger_print(&ed->logger, LOG_OUTPUT,
      "  Memory size: %lu Bytes\n", (unsigned long)desc.mem_size);
  } else {
    logger_print(&ed->logger, LOG_OUTPUT,
      "  Memory size: %.3f MB\n", (double)desc.mem_size/(1024.0*1024.0));
  }

  if(desc.lower[0] >= desc.upper[0]
  || desc.lower[1] >= desc.upper[1]
  || desc.lower[2] >= desc.upper[2]) {
    logger_print(&ed->logger, LOG_OUTPUT,
      "  Axis Aligned Bounding Box: `invalid'\n");
  } else {
    logger_print(&ed->logger, LOG_OUTPUT,
      "  Axis Aligned Bounding Box: %.2f:%.2f:%.2f %.2f:%.2f:%.2f\n",
      SPLIT3(desc.lower),
      SPLIT3(desc.upper));
  }
  logger_print(&ed->logger, LOG_OUTPUT, "  SVO definition: %ux%ux%u\n",
    desc.definition, desc.definition, desc.definition);
  return RES_OK;
}

res_T
vxed_cmd_func_show_video_mode(struct vxed* ed, char* opt, void* ctx)
{
  unsigned vmode[3];
  ASSERT(ed);
  (void)ctx, (void)opt;
  vxed_window_get_video_mode(ed->win, vmode);
  logger_print(&ed->logger, LOG_OUTPUT,
    "video_mode = %ux%u@%ubpp\n", SPLIT3(vmode));
  return RES_OK;
}

res_T
vxed_cmd_func_show_vxl_definition(struct vxed* ed, char* opt, void* ctx)
{
  ASSERT(ed);
  (void)ctx, (void)opt;
  logger_print(&ed->logger, LOG_OUTPUT,
    "vxl_definition = %lu\n", (unsigned long)ed->definition);
  return RES_OK;
}

res_T
vxed_cmd_func_show_vxl_LOD_min(struct vxed* ed, char* opt, void* ctx)
{
  ASSERT(ed);
  (void)ctx, (void)opt;
  logger_print(&ed->logger, LOG_OUTPUT,
    "vxl_LOD_min = %u\n", ed->vxr_draw_state.rt_state.lod_min);
  return RES_OK;
}

res_T
vxed_cmd_func_show_vxl_LOD_max(struct vxed* ed, char* opt, void* ctx)
{
  ASSERT(ed);
  (void)ctx, (void)opt;
  logger_print(&ed->logger, LOG_OUTPUT,
    "vxl_LOD_max = %u\n", ed->vxr_draw_state.rt_state.lod_max);
  return RES_OK;
}

res_T
vxed_cmd_func_show_vxl_rt_coherency(struct vxed* ed, char* opt, void* ctx)
{
  const char* coherency = NULL;
  ASSERT(ed);
  (void)ctx, (void)opt;

  switch(ed->vxr_draw_state.rt_state.coherency) {
    case VXL_RT_COHERENCY_NONE: coherency = "none"; break;
    case VXL_RT_COHERENCY_MIXED: coherency = "mixed"; break;
    case VXL_RT_COHERENCY_LOW: coherency = "low"; break;
    case VXL_RT_COHERENCY_MEDIUM: coherency = "medium"; break;
    case VXL_RT_COHERENCY_HIGH: coherency = "high"; break;
    default: FATAL("Unreachable code\n"); break;
  }
  logger_print(&ed->logger, LOG_OUTPUT,
    "vxl_rt_coherency = %s\n", coherency);
  return RES_OK;
}

res_T
vxed_cmd_func_show_vxl_rt_simd(struct vxed* ed, char* opt, void* ctx)
{
  ASSERT(ed);
  (void)ctx, (void)opt;
  logger_print(&ed->logger, LOG_OUTPUT,
    "vxl_rt_simd = %d\n", (ed->vxr_draw_state.rt_state.simd) != 0);
  return RES_OK;
}

res_T
vxed_cmd_func_show_vxr_beam_prepass(struct vxed* ed, char* opt, void* ctx)
{
  ASSERT(ed);
  (void)ctx, (void)opt;
  logger_print(&ed->logger, LOG_OUTPUT,
    "vxr_beam_prepass = %d\n",
    (ed->vxr_draw_state.mask & VXR_DRAW_BEAM_PREPASS) != 0);
  return RES_OK;
}

res_T
vxed_cmd_func_show_vxr_render_type(struct vxed* ed, char* opt, void* ctx)
{
  const char* rtype;
  ASSERT(ed);
  (void)ctx, (void)opt;

  switch(ed->vxr_draw_state.render_type) {
    case VXR_RENDER_COLOR: rtype = "color"; break;
    case VXR_RENDER_CUBIC: rtype = "cubic"; break;
    case VXR_RENDER_LEGACY: rtype = "legacy"; break;
    case VXR_RENDER_NORMAL: rtype = "normal"; break;
    default: FATAL("Unreachable code\n"); break;
  }
  logger_print(&ed->logger, LOG_OUTPUT, "vxr_render_type = %s\n", rtype);
  return RES_OK;
}

res_T
vxed_cmd_func_show_warranty(struct vxed* ed, char* opt, void* ctx)
{
  (void)opt, (void)ctx;
  return vxed_print_text(ed, g_warranty, sizeof(g_warranty)/sizeof(const char*));
}

