/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXED_PARSE_H
#define VXED_PARSE_H

#include "vxed.h"
#include <rsys/rsys.h>

#include <limits.h>
#include <string.h>

/*******************************************************************************
 * Helper functions
 * TODO remove this function and use the cstr_to_xxx RSys alternative
 ******************************************************************************/
static INLINE res_T
vxed_str_to_float(const char* str, float* dst)
{
  char* end;
  ASSERT(dst);
  if(!str)
    return RES_BAD_ARG;
  *dst = (float)strtod(str, &end);
  if(end == str || *end != '\0')
    return RES_BAD_ARG;
  return RES_OK;
}

static INLINE res_T
vxed_str_to_long(const char* str, long* dst)
{
  char* end;
  ASSERT(dst);
  if(!str)
    return RES_BAD_ARG;
  if(!strcmp(str, "INT_MAX")) {
    *dst = INT_MAX;
  } else {
    *dst = strtol(str, &end, 10/* base */);
    if(end == str || *end != '\0')
      return RES_BAD_ARG;
  }
  return RES_OK;
}

static INLINE res_T
vxed_str_to_int(const char* str, int* dst)
{
  long l;
  res_T res;
  ASSERT(dst);
  res = vxed_str_to_long(str, &l);
  if(res != RES_OK)
    return res;
  if(l > INT_MAX || l < INT_MIN)
    return RES_BAD_ARG;
  *dst = (int)l;
  return RES_OK;
}

static INLINE res_T
vxed_str_to_uint(const char* str, unsigned* dst)
{
  long l;
  res_T res;
  ASSERT(dst);
  res = vxed_str_to_long(str, &l);
  if(res != RES_OK)
    return res;
  if(l > (long)UINT_MAX || l < 0)
    return RES_BAD_ARG;
  *dst = (unsigned)l;
  return RES_OK;
}

BEGIN_DECLS

/*******************************************************************************
 * Parse functions
 ******************************************************************************/
VXED_API res_T
vxed_parse_float3
  (const char* str,
   float dst[3]);

VXED_API res_T
vxed_parse_video_mode
  (const char* str,
   unsigned vmode[3]); /* Width, height, Bits Per Pixel */

VXED_API res_T
vxed_parse_fov
  (const char* str,
   float* fov);

END_DECLS

#endif /* PARSE_H */
