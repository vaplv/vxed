/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXED_SCENE_H
#define VXED_SCENE_H

#include "vxed.h"

#include <rsys/rsys.h>
#include <vx/vxl.h>

struct vxed_model;
struct vxed_scene;
struct vxl_scene;
struct vx;

VXED_API res_T
vxed_scene_create
  (struct vxed* ed,
   struct vxed_scene** scn);

VXED_API void
vxed_scene_ref_get
  (struct vxed_scene* scn);

VXED_API void
vxed_scene_ref_put
  (struct vxed_scene* scn);

VXED_API res_T
vxed_scene_load
  (struct vxed_scene* scn,
   const size_t definition, /* # voxels in X, Y and Z */
   const enum vxl_mem_location memloc,
   const char* filename);

VXED_API struct vxl_scene*
vxed_scene_get_vxl_scene
  (const struct vxed_scene* scn);

VXED_API void
vxed_scene_clear
  (struct vxed_scene* scn);

#endif /* SCENE_H */

