/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxed.h"
#include "vxed_app.h"
#include "vxed_args.h"

int
main(int argc, char** argv)
{
  struct vxed_args args = VXED_ARGS_DEFAULT;
  struct vxed* ed = NULL;
  struct mem_allocator allocator_proxy;
  int quit;
  res_T res = RES_OK;

  mem_init_proxy_allocator(&allocator_proxy, &mem_default_allocator);

  res = vxed_args_parse(&args, &quit, argc, argv);
  if(res != RES_OK) return res;
  if(quit) return RES_OK;
  res = vxed_create(&allocator_proxy, &args, &ed);
  if(res != RES_OK) return res;
  res = vxed_run(ed);
  if(res != RES_OK) goto error;

exit:
  if(ed) VXED(release(ed));
  if(MEM_ALLOCATED_SIZE(&allocator_proxy)) {
    char dump[1024];
    MEM_DUMP(&allocator_proxy, dump, sizeof(dump)/sizeof(char));
    fprintf(stderr, "Memory leaks: %s\n", dump);
  }
  mem_shutdown_proxy_allocator(&allocator_proxy);
  if(mem_allocated_size() != 0) {
    fprintf(stderr, "Untracked memory leaks: %lu Bytes\n",
      (unsigned long)mem_allocated_size());
    res = RES_MEM_ERR;
  }
  return res;
error:
  goto exit;
}
