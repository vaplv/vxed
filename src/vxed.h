/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXED_H
#define VXED_H

#include <rsys/logger.h>
#include <rsys/mem_allocator.h>
#include <rsys/rsys.h>

#include <vx/vxl.h>
#include <vx/vxr.h>

#include <SDL.h>

/* Library symbol management */
#if defined(VXED_SHARED_BUILD) /* Build shared library */
  #define VXED_API extern EXPORT_SYM
#elif defined(VXED_STATIC)  /* Use/build static library */
  #define VXED_API extern LOCAL_SYM
#else /* Use shared library */
  #define VXED_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the vxed function `Func'
 * returns an error. One should use this macro on vxed function calls for which
 * no explicit error checking is performed. */
#ifndef NDEBUG
  #define VXED(Func) ASSERT(vxed_ ## Func == RES_OK)
#else
  #define VXED(Func) vxed_ ## Func
#endif

/* Helper macro defining the body of the ref counting functions on Type. This
 * macro assumes that the struct Type has an internal ref_T field named `ref'
 * and that a `void Type##_release(ref_T*)' function exists */
#define VXED_DEFINE_REF_FUNCS(Type)                                            \
  void                                                                         \
  Type ## _ref_get(struct Type* p)                                             \
  {                                                                            \
    ASSERT(p);                                                                 \
    ref_get(&p->ref);                                                          \
  }                                                                            \
  void                                                                         \
  Type ## _ref_put(struct Type* p)                                             \
  {                                                                            \
    ASSERT(p);                                                                 \
    ref_put(&p->ref, Type ## _release);                                        \
  }

struct vxed;
struct vxed_args;
struct mem_allocator;

enum vxed_state {
  VXED_COMMAND, /* Challenge commands */
  VXED_EXIT /* Shutdown the application */
};

enum option {
  OPT_PRINT_LOAD_PERF,
  OPT_PRINT_RENDER_PERF,
  OPT_COUNT__
};

struct vxed {
  struct vxaw* vxaw;
  struct vxl_system* vxl; /* Manage the voxelised models */
  struct vxr_device* vxr; /* Use to render a VXL scene */
  struct vxr_draw_desc vxr_draw_state; /* Current rendering state */

  struct vxed_input* input; /* Main VxEd input */
  struct vxed_scene* scene; /* Main VxEd scene  */
  struct vxed_view* view; /* Main VxEd view */
  struct vxed_window* win; /* Main VxEd window */

  struct list_node cmds; /* List of built-in commands */

  int options[OPT_COUNT__]; /* List of VxEd options */
  float camera_speed_rotation; /* Speed rotation of the camera */
  float camera_speed_translation; /* Speed translation of the camera */
  enum vxed_state state; /* Current state of the VxEd application */
  size_t definition; /* # voxels along the X, Y and Z axis */
  enum vxl_mem_location mem_location; /* Memory Location of the voxels */

  unsigned nthreads;

  struct mem_allocator* allocator;
  struct logger logger;
};

extern LOCAL_SYM res_T
vxed_set_verbose
  (struct vxed* ed,
   const int verbosity);

BEGIN_DECLS

/*******************************************************************************
 * VXED main functions
 ******************************************************************************/
/* Wait for user inputs. Set `yes_no' to 1 if "Y" or "y" is entered */
VXED_API res_T
vxed_prompt_yes_no
  (struct vxed* ed,
   int* yes_no);

/* Print a text formatted as a list of lines. `content' is a list of `nlines'
 * lines of text. It is split in several parts with respect to the terminal
 * size. Note that for the best portability, the length of each line should
 * not exceed 80 characters */
VXED_API res_T
vxed_print_text
  (struct vxed* ed,
   const char* content[],
   const size_t nlines);

VXED_API res_T
vxed_draw
  (struct vxed* ed);

END_DECLS

#endif /* VXED_H */

