/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxed.h"
#include "vxed_app.h"
#include "vxed_args.h"
#include "vxed_cmd.h"
#include "vxed_conf.h"
#include "vxed_input.h"
#include "vxed_scene.h"
#include "vxed_view.h"
#include "vxed_window.h"

#include <omp.h>

#include <readline/readline.h>
#include <readline/history.h>

#include <rsys/clock_time.h>

#include <vx/vxaw.h>
#include <vx/vxl.h>
#include <vx/vxr.h>

/* Only one instance of VxEd can be launched. This global variable point toward
 * the current VxEd instance */
static struct vxed* g_vxed = NULL;

/* HACK uses to send variable to the readline completion functions since its
 * API does not allow local context */
static struct list_node* g_cmds;

#define VXED_PS1 "(vxed) $ "

/*******************************************************************************
 * Command helper functions
 ******************************************************************************/
static char*
complete_cmd(const char* text, int status/*0 on 1st invocation*/)
{
  static size_t text_len;
  static struct list_node* node;

  if(!g_cmds)
    return rl_filename_completion_function(text, status);

  if(!status) {
    text_len = strlen(text);
    node = g_cmds->next;
  }

  while(node != g_cmds) {
    struct vxed_cmd* cmd = CONTAINER_OF(node, struct vxed_cmd, node);
    if(strncmp(cmd->name, text, text_len)) {
      node = node->next;
    } else {
      char* entry = malloc(strlen(cmd->name) + 1);
      if(entry) strcpy(entry, cmd->name);
      node = node->next;
      return entry;
    }
  }
  return rl_filename_completion_function(text, status);
}

static FINLINE char**
completion_matches_cmd(const char* text, struct list_node* cmds)
{
  char** list;
  g_cmds = cmds; /* HACK */
  list = rl_completion_matches(text, complete_cmd);
  g_cmds = NULL; /* HACK */
  return list;
}

static char**
completion_matches_sub_cmds
  (const char* text,
   const struct list_node* cmds,
   const size_t ibegin,
   const size_t iend)
{
  size_t icompletion = 0;
  const struct list_node* node;
  ASSERT(text && cmds && ibegin < iend);
  LIST_FOR_EACH(node, cmds) {
    struct vxed_cmd* cmd = CONTAINER_OF(node, struct vxed_cmd, node);
    const char* name = cmd->name;
    const size_t name_len = strlen(name);

    if((size_t)(iend - ibegin) >= name_len + 1/*space*/
    && !strncmp(rl_line_buffer + ibegin, name, name_len)
    && (  rl_line_buffer[ibegin + name_len] == ' '
       || rl_line_buffer[ibegin + name_len] == '\t')) {
      size_t ichar;

      if(is_list_empty(&cmd->sub_cmds)) continue;

      FOR_EACH(ichar, ibegin + name_len + 1/*space*/, iend) { /* Skip spaces */
        if(rl_line_buffer[ichar] != ' ' && rl_line_buffer[ichar] != '\t')
          break;
      }

      if(ichar == iend) { /* Complete the sub commands */
        char** result = completion_matches_cmd(text, &cmd->sub_cmds);
        if(result) {
          rl_attempted_completion_over = 1;
        } else  {
          result = rl_completion_matches(text, rl_filename_completion_function);
        }
        return result;
      } else {
        /* Recursively complete the args of the command arg */
        return completion_matches_sub_cmds(text, &cmd->sub_cmds, ichar, iend);
      }
    }
    ++icompletion;
  }
  return rl_completion_matches(text, rl_filename_completion_function);
}

static char**
completion_matches(const char* text, int start, int end)
{
  int i;
  (void)end;

  FOR_EACH(i, 0, start) /* Skip spaces */
    if(rl_line_buffer[i] != ' ' && rl_line_buffer[i] != '\t') break;

  if(start == 0 || i == start) {
    rl_attempted_completion_over = 1;
    return completion_matches_cmd(text, &g_vxed->cmds);
  } else {
    return completion_matches_sub_cmds(text, &g_vxed->cmds, (size_t)i, (size_t)start);
  }
}

static res_T
challenge_command(struct vxed* ed)
{
  char* line = NULL;
  res_T res = RES_OK;

  rl_attempted_completion_function = completion_matches;
  line = readline(VXED_PS1);
  if(line && *line) {
    add_history(line);
    res = vxed_cmd_exec(ed, &ed->cmds, line);
  }
  free(line);
  return res;
}

static void
log_warn(const char* msg, void* ctx)
{
  (void)ctx;
  fprintf(stderr, "\x1B[1;33mwarning:\x1B[0m %s", msg);
  fflush(stderr);
}

static void
log_err(const char* msg, void* ctx)
{
  (void)ctx;
  fprintf(stderr, "\x1B[1;31merror:\x1B[0m %s", msg);
  fflush(stderr);
}

static void
log_out(const char* msg, void* ctx)
{
  (void)ctx;
  fprintf(stdout, "%s", msg);
  fflush(stdout);
}

static void
init_build_in_cmds(struct vxed* vxed)
{
  ASSERT(vxed);
  /* Register the main commands */
  #define VXED_CMD(Name, Doc)                                                  \
    VXED(cmd_register(&vxed->cmds, &vxed_cmd_ ## Name));                       \
    VXED(cmd_register(&vxed_cmd_help.sub_cmds, &vxed_cmd_help_ ## Name));
  #include "vxed_cmd_decl.h"
  /* Register the show commands */
  #define VXED_CMD(Name, Doc)                                                  \
    VXED(cmd_register(&vxed_cmd_show.sub_cmds, &vxed_cmd_show_ ## Name));      \
    VXED(cmd_register(&vxed_cmd_help_show.sub_cmds, &vxed_cmd_help_show_ ## Name));
  #include "vxed_cmd_show_decl.h"
  /* Register the cvar commands */
  #define VXED_CMD(Name, Doc)                                                  \
    VXED(cmd_register(&vxed_cmd_set.sub_cmds, &vxed_cmd_set_ ## Name));        \
    VXED(cmd_register(&vxed_cmd_show.sub_cmds, &vxed_cmd_show_ ## Name));      \
    VXED(cmd_register(&vxed_cmd_help_set.sub_cmds, &vxed_cmd_help_set_ ## Name));\
    VXED(cmd_register(&vxed_cmd_help_show.sub_cmds, &vxed_cmd_help_show_ ## Name));
  #include "vxed_cvar_decl.h"
  /* Set vxl_rt_coherency sub-commands */
  VXED(cmd_register(&vxed_cmd_set_vxl_rt_coherency.sub_cmds, &vxed_cmd_set_vxl_rt_coherency_high));
  VXED(cmd_register(&vxed_cmd_set_vxl_rt_coherency.sub_cmds, &vxed_cmd_set_vxl_rt_coherency_medium));
  VXED(cmd_register(&vxed_cmd_set_vxl_rt_coherency.sub_cmds, &vxed_cmd_set_vxl_rt_coherency_low));
  VXED(cmd_register(&vxed_cmd_set_vxl_rt_coherency.sub_cmds, &vxed_cmd_set_vxl_rt_coherency_mixed));
  VXED(cmd_register(&vxed_cmd_set_vxl_rt_coherency.sub_cmds, &vxed_cmd_set_vxl_rt_coherency_none));
  VXED(cmd_register(&vxed_cmd_help_set_vxl_rt_coherency.sub_cmds, &vxed_cmd_help_set_vxl_rt_coherency_high));
  VXED(cmd_register(&vxed_cmd_help_set_vxl_rt_coherency.sub_cmds, &vxed_cmd_help_set_vxl_rt_coherency_medium));
  VXED(cmd_register(&vxed_cmd_help_set_vxl_rt_coherency.sub_cmds, &vxed_cmd_help_set_vxl_rt_coherency_low));
  VXED(cmd_register(&vxed_cmd_help_set_vxl_rt_coherency.sub_cmds, &vxed_cmd_help_set_vxl_rt_coherency_mixed));
  VXED(cmd_register(&vxed_cmd_help_set_vxl_rt_coherency.sub_cmds, &vxed_cmd_help_set_vxl_rt_coherency_none));
}

static void
release_built_in_cmds(void)
{
  /* Unregister the main commands */
  #define VXED_CMD(Name, Doc)                                                  \
    VXED(cmd_unregister(&vxed_cmd_ ## Name));                                  \
    VXED(cmd_unregister(&vxed_cmd_help_ ## Name));
  #include "vxed_cmd_decl.h"
  /* Unregister the show commands */
  #define VXED_CMD(Name, Doc)                                                  \
    VXED(cmd_unregister(&vxed_cmd_show_ ## Name));                             \
    VXED(cmd_unregister(&vxed_cmd_help_show_ ## Name));
  #include "vxed_cmd_show_decl.h"
  /* Unregister the cvar commands */
  #define VXED_CMD(Name, Doc)                                                  \
    VXED(cmd_unregister(&vxed_cmd_set_ ## Name));                              \
    VXED(cmd_unregister(&vxed_cmd_show_ ## Name));                             \
    VXED(cmd_unregister(&vxed_cmd_help_set_ ## Name));                         \
    VXED(cmd_unregister(&vxed_cmd_help_show_ ## Name));
  #include "vxed_cvar_decl.h"
  /* Unregister the set vxl_rt_coherency sub-commands */
  VXED(cmd_unregister(&vxed_cmd_set_vxl_rt_coherency_high));
  VXED(cmd_unregister(&vxed_cmd_set_vxl_rt_coherency_medium));
  VXED(cmd_unregister(&vxed_cmd_set_vxl_rt_coherency_low));
  VXED(cmd_unregister(&vxed_cmd_set_vxl_rt_coherency_mixed));
  VXED(cmd_unregister(&vxed_cmd_help_set_vxl_rt_coherency_high));
  VXED(cmd_unregister(&vxed_cmd_help_set_vxl_rt_coherency_medium));
  VXED(cmd_unregister(&vxed_cmd_help_set_vxl_rt_coherency_low));
  VXED(cmd_unregister(&vxed_cmd_help_set_vxl_rt_coherency_mixed));
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
vxed_create
  (struct mem_allocator* allocator,
   const struct vxed_args* args,
   struct vxed** out_vxed)
{
  struct vxed* ed = NULL;
  struct mem_allocator* mem_allocator;
  res_T res = RES_OK;
  ASSERT(out_vxed && args);

  if(!args || !out_vxed) {
    log_err("Invalid argument used to create a VxEd instance.\n", NULL);
    res = RES_BAD_ARG;
    goto error;
  }

  if(g_vxed) {
    log_err("An instance of VxEd is already running.\n", NULL);
    res = RES_BAD_OP;
    goto error;
  }

  mem_allocator = allocator ? allocator : &mem_default_allocator;

  ed = MEM_CALLOC(mem_allocator, 1, sizeof(struct vxed));
  if(!ed) {
    log_err("Not enough memory space to create VxEd\n", NULL);
    res = RES_MEM_ERR;
    goto error;
  }
  ed->allocator = mem_allocator;
  ed->state = VXED_COMMAND;
  ed->camera_speed_rotation = 0.001f;
  ed->camera_speed_translation = 1.f;
  ed->definition = (size_t)args->definition;
  ed->mem_location = args->mem_location;
  ed->vxr_draw_state = VXR_DRAW_DESC_DEFAULT;
  ed->nthreads = args->nthreads_hint;

  list_init(&ed->cmds);

  logger_init(ed->allocator, &ed->logger);
  logger_set_stream(&ed->logger, LOG_OUTPUT, log_out, NULL);
  logger_set_stream(&ed->logger, LOG_ERROR, log_err, NULL);
  logger_set_stream(&ed->logger, LOG_WARNING, log_warn, NULL);

  logger_print(&ed->logger,  LOG_OUTPUT,
"vxed Copyright (C) 2014-2018 Vincent Forest\n"
"This program comes with ABSOLUTELY NO WARRANTY; for details type `show warranty'.\n"
"This is free software, and you are welcome to redistribute it under certain\n"
"conditions; type `show copying' for details.\n\n");

  init_build_in_cmds(ed);

  res = vxl_system_create(ed->allocator, args->nthreads_hint, &ed->vxl);
  if(res != RES_OK) {
    logger_print(&ed->logger, LOG_ERROR,
      "Could not create the VoXel Library system.\n");
    goto error;
  }
  res = vxr_device_create(ed->allocator, ed->vxl, args->nthreads_hint, &ed->vxr);
  if(res != RES_OK) {
    logger_print(&ed->logger, LOG_ERROR,
      "Couldn't create the VoXel Renderer device\n");
    goto error;
  }
  res = vxaw_create(&ed->logger, ed->allocator, NULL, NULL, ed->vxl, &ed->vxaw);
  if(res != RES_OK) {
    logger_print(&ed->logger, LOG_ERROR,
      "Couldn't create the VoXelizer of Alias Wavefront obj\n");
    goto error;
  }

  if(SDL_Init(SDL_INIT_VIDEO)) {
    logger_print(&ed->logger, LOG_ERROR,
      "Could not initialize SDL: %s\n", SDL_GetError());
    res = RES_UNKNOWN_ERR;
    goto error;
  }

  res = vxed_window_create(ed, args->video_mode, &ed->win);
  if(res != RES_OK) goto error;
  res = vxed_view_create(ed, &ed->view);
  if(res != RES_OK) goto error;
  res = vxed_scene_create(ed, &ed->scene);
  if(res != RES_OK) goto error;
  res = vxed_input_create(ed, VXED_INPUT_DEFAULT_KEY_BINDING,
    VXED_INPUT_DEFAULT_BUTTON_BINDING, &ed->input);
  if(res != RES_OK) goto error;

  res = vxed_view_setup(ed->view, args->view_position, args->view_target,
    args->view_up, (float)args->video_mode[0]/(float)args->video_mode[1],
    args->view_fov);
  if(res != RES_OK) goto error;

  vxed_view_attach_to_window(ed->view, ed->win);

  /* Load the configuration file */
  if(args->conf_filename) {
    res = vxed_conf_load(ed, args->conf_filename);
    if(res != RES_OK) goto error;
  }

  if(args->model) {
    /* Import the sumbitted model */
    vxed_cmd_load.func(ed, args->model, vxed_cmd_load.context);
  }

exit:
  if(out_vxed) *out_vxed = ed;
  g_vxed = ed;
  return res;
error:
  if(ed) {
    VXED(release(ed));
    ed = NULL;
  }
  goto exit;
}

res_T
vxed_release(struct vxed* ed)
{
  if(!ed) return RES_BAD_ARG;

  release_built_in_cmds();

  if(ed->vxl) VXL(system_ref_put(ed->vxl));
  if(ed->vxr) VXR(device_ref_put(ed->vxr));
  if(ed->vxaw) VXAW(ref_put(ed->vxaw));
  if(ed->scene) vxed_scene_ref_put(ed->scene);
  if(ed->view) vxed_view_ref_put(ed->view);
  if(ed->win) vxed_window_ref_put(ed->win);
  if(ed->input) vxed_input_ref_put(ed->input);
  SDL_Quit();
  logger_release(&ed->logger);
  MEM_RM(ed->allocator, ed);
  g_vxed = NULL;
  return RES_OK;
}

res_T
vxed_run(struct vxed* ed)
{
  if(!ed) return RES_BAD_ARG;
  while(ed->state != VXED_EXIT)
    challenge_command(ed);
  return RES_OK;
}

