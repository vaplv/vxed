/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXED_CMD_H
#define VXED_CMD_H

#include "vxed.h"
#include <rsys/rsys.h>
#include <rsys/list.h>

/* Command of the vxed application */
struct vxed_cmd {
  const char* name; /* Name of the command */
  const char* doc; /* Brief command summary. Should not exceed 80 chars */
  /* Function to invokes on the execution of the command. `arg contains the
   * remaining command arguments following the command `name'. `ctx' is a
   * pointer toward the `context' of the command */
  res_T (*func)(struct vxed* ed, char* opt, void* ctx);
  void* context; /* May be NULL */
  struct list_node sub_cmds; /* Linked list of sub commands */

  /* Internal data. Must not be used */
  struct list_node node;
};

/* Define if a vxed_cmd is NULL */
#define VXED_CMD_IS_NULL(Cmd) ((Cmd)->Name == NULL)

#define VXED_CMD_NULL__ {NULL, NULL, NULL, NULL, {NULL, NULL}, {NULL, NULL}}
static const struct vxed_cmd VXED_CMD_NULL = VXED_CMD_NULL__;

/* Declare and initialise a vxed_cmd instance named `Var'. Work properly for
 * global variables but should warn if used on local variables in a C89
 * code-base. In C89 standard, initializer list must be constant compile time
 * expressions while some of of the declared sbox_cmd fields are initialised
 * with the address of the sbox_cmd */
#define VXED_CMD_DECLARE(Var, Name, Doc, Func, Ctx)                            \
  struct vxed_cmd Var = {                                                      \
    Name, Doc, Func, Ctx,                                                      \
    {&(Var).sub_cmds, &(Var).sub_cmds },                                       \
    {&(Var).node, &(Var).node }                                                \
  }

BEGIN_DECLS

/*******************************************************************************
 * Built-in commands declaration
 ******************************************************************************/
/* Main built-in commands */
#define VXED_CMD(Name, Doc)                                                    \
  VXED_API struct vxed_cmd vxed_cmd_ ## Name;                                  \
  VXED_API struct vxed_cmd vxed_cmd_help_ ## Name;
#include "vxed_cmd_decl.h"

/* CVars built-in commands */
#define VXED_CMD(Name, Doc)                                                    \
  VXED_API struct vxed_cmd vxed_cmd_set_ ## Name;                              \
  VXED_API struct vxed_cmd vxed_cmd_show_ ## Name;                             \
  VXED_API struct vxed_cmd vxed_cmd_help_set_ ## Name;                         \
  VXED_API struct vxed_cmd vxed_cmd_help_show_ ## Name;
#include "vxed_cvar_decl.h"

/* Show built-in commands */
#define VXED_CMD(Name, Doc)                                                   \
  VXED_API struct vxed_cmd vxed_cmd_show_ ## Name;                            \
  VXED_API struct vxed_cmd vxed_cmd_help_show_ ## Name;
#include "vxed_cmd_show_decl.h"

#define CMD_DECLARE__(Name) VXED_API struct vxed_cmd vxed_cmd_ ## Name;

/* Set vxl_accel_type sub-commands */
CMD_DECLARE__(set_vxl_accel_type_bvh)
CMD_DECLARE__(set_vxl_accel_type_svo)
CMD_DECLARE__(help_set_vxl_accel_type_bvh)
CMD_DECLARE__(help_set_vxl_accel_type_svo)

/* Set vxl_rt_coherency sub-commands */
CMD_DECLARE__(set_vxl_rt_coherency_high)
CMD_DECLARE__(set_vxl_rt_coherency_medium)
CMD_DECLARE__(set_vxl_rt_coherency_low)
CMD_DECLARE__(set_vxl_rt_coherency_mixed)
CMD_DECLARE__(set_vxl_rt_coherency_none)
CMD_DECLARE__(help_set_vxl_rt_coherency_high)
CMD_DECLARE__(help_set_vxl_rt_coherency_medium)
CMD_DECLARE__(help_set_vxl_rt_coherency_low)
CMD_DECLARE__(help_set_vxl_rt_coherency_mixed)
CMD_DECLARE__(help_set_vxl_rt_coherency_none)

#undef CMD_DECLARE__

/*******************************************************************************
 * Commadn API
 ******************************************************************************/
VXED_API res_T
vxed_cmd_init
  (struct vxed_cmd* cmd,
   const char* name,
   const char* doc,
   res_T (*func)(struct vxed* vxed, char* arg, void* ctx),
   void* context);

/* Register `cmd' into `cmds_list'. If `cmd' is already registered, it is
 * priorly unregistered from its previous command list */
VXED_API res_T
vxed_cmd_register
  (struct list_node* cmds_list,
   struct vxed_cmd* cmd);

/* Remove cmd from the commands list in which it is registered. */
VXED_API res_T
vxed_cmd_unregister
  (struct vxed_cmd* cmd);

/* Print all commands listed in `cmds_list'. `iline' is the number of lines
 * already printed in the terminal that have to be visible on the screen with
 * the list of commands */
VXED_API res_T
vxed_cmd_print
  (struct vxed* ed,
   const struct list_node* cmds_list,
   const size_t iline);

/* Look for a valid command in cmds that match the first argument of
 * cmd_string. If found, execute the corresponding command function on the
 * remainings cmd_string arguments */
VXED_API res_T
vxed_cmd_exec
  (struct vxed* ed,
   const struct list_node* cmds,
   char* cmd_string);

END_DECLS

#endif /* VXED_CMD_H */

