/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxed.h"
#include "vxed_c.h"
#include "vxed_args.h"
#include "vxed_scene.h"
#include "vxed_view.h"
#include "vxed_window.h"

#include <rsys/clock_time.h>
#include <rsys/float3.h>
#include <rsys/logger.h>
#include <rsys/math.h>

#include <vx/vxaw.h>
#include <vx/vxl.h>
#include <vx/vxr.h>

#include <limits.h>
#include <readline/readline.h>

/*******************************************************************************
 * Local functions
 ******************************************************************************/
res_T
vxed_set_verbose(struct vxed* ed, const int verb)
{
  ASSERT(ed);
  ed->options[OPT_PRINT_LOAD_PERF] = verb;
  ed->options[OPT_PRINT_RENDER_PERF] = verb;
  return RES_OK;
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
int
vxed_prompt_continue(struct vxed* ed)
{
  int opt, next;

  ASSERT(ed);
  logger_print(&ed->logger, LOG_OUTPUT,
    "\x1B[7m<return> to continue, or q <return> to quit\x1B[0m");
  opt = getc(stdin);
  next = opt != 'q';
  if(opt != '\n') /* `Flush' stdin */
    while(getc(stdin) != '\n');
  return next;
}

res_T
vxed_prompt_yes_no(struct vxed* vxed, int* yes_no)
{
  int val;
  if(!vxed || !yes_no) return RES_BAD_ARG;
  val = getc(stdin);
  *yes_no = (val == 'y' || val == 'Y');
  if(val != '\n'|| val != '\r') while(getc(stdin) != '\n'); /* Flush stdin */
  return RES_OK;
}

res_T
vxed_print_text(struct vxed* vxed, const char* content[], const size_t nlines)
{
  int width, height;
  size_t iline, iline_scr;

  if(!vxed || (nlines && !content))
     return RES_BAD_ARG;

  rl_get_screen_size(&height, &width);
  iline_scr = 0;
  FOR_EACH(iline, 0, nlines) {
    if(iline_scr < (size_t)(height - 1)) {
      logger_print(&vxed->logger, LOG_OUTPUT, "%s", content[iline]);
      ++iline_scr;
    } else {
      if(!vxed_prompt_continue(vxed)) {
        iline = nlines;
      } else {
        iline_scr = 0;
      }
    }
  }
  return RES_OK;
}

res_T
vxed_draw(struct vxed* ed)
{
  struct time t0, t1;
  struct vxr_camera* cam;
  struct vxr_framebuffer* fbuf;
  struct vxl_scene* scn;
  res_T res;

  scn = vxed_scene_get_vxl_scene(ed->scene);
  res = vxed_view_get_vxr_camera(ed->view, &cam);
  if(res != RES_OK) return res;
  res = vxed_window_get_vxr_framebuffer(ed->win, &fbuf);
  if(res != RES_OK) return res;

  if(ed->options[OPT_PRINT_RENDER_PERF]) time_current(&t0);

  res = vxr_draw(ed->vxr, &ed->vxr_draw_state, scn, cam, fbuf);
  if(res != RES_OK) {
    logger_print(&ed->logger, LOG_ERROR, "Draw error.\n");
    return res;
  }

  if(ed->options[OPT_PRINT_RENDER_PERF]) {
    char dump[64];
    int64_t msec;
    time_current(&t1);
    time_sub(&t0, &t1, &t0);
    time_dump(&t0, TIME_MSEC|TIME_USEC, NULL, dump, sizeof(dump));
    msec = time_val(&t0, TIME_MSEC);
    if(!msec) {
      logger_print(&ed->logger, LOG_OUTPUT, "Draw time: %s rays/s\n", dump);
    } else {
      unsigned vmode[3];
      vxed_window_get_video_mode(ed->win, vmode);
      logger_print(&ed->logger, LOG_OUTPUT, "Draw time: %s - %.2f M rays/s\n",
        dump, (float)((vmode[0]*vmode[1]*1000) / msec)*0.000001);
    }
  }
  vxed_window_swap(ed->win);
  return RES_OK;
}
