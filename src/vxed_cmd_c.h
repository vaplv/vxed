/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXED_CMD_C_H
#define VXED_CMD_C_H

#include "vxed_cmd.h"

/* Functions of main built-in commands */
#define VXED_CMD(Name, Doc)                                                    \
  extern LOCAL_SYM res_T                                                       \
  vxed_cmd_func_ ## Name(struct vxed* vxed, char* arg, void* ctx);             \
  extern LOCAL_SYM res_T                                                       \
  vxed_cmd_func_help_ ## Name(struct vxed* vxed, char* arg, void* ctx);
#include "vxed_cmd_decl.h"

/* Functions of show built-in commands */
#define VXED_CMD(Name, Doc)                                                    \
  extern LOCAL_SYM res_T                                                       \
  vxed_cmd_func_show_ ## Name(struct vxed* vxed, char* arg, void* ctx);        \
  extern LOCAL_SYM res_T                                                       \
  vxed_cmd_func_help_show_ ## Name(struct vxed* vxed, char* arg, void* ctx);
#include "vxed_cmd_show_decl.h"

/* Functions of CVars built-in commands */
#define VXED_CMD(Name, Doc)                                                    \
  extern LOCAL_SYM res_T                                                       \
  vxed_cmd_func_set_ ## Name(struct vxed* vxed, char* arg, void* ctx);         \
  extern LOCAL_SYM res_T                                                       \
  vxed_cmd_func_help_set_ ## Name(struct vxed* vxed, char* arg, void* ctx);    \
  extern LOCAL_SYM res_T                                                       \
  vxed_cmd_func_show_ ## Name(struct vxed* vxed, char* arg, void* ctx);        \
  extern LOCAL_SYM res_T                                                       \
  vxed_cmd_func_help_show_ ## Name(struct vxed* vxed, char* arg, void* ctx);
#include "vxed_cvar_decl.h"

/* Helper macro use to make the VxEd cmd declaration less verbose */
#define CMD_DECLARE__(Name)                                                    \
  extern LOCAL_SYM res_T                                                       \
  vxed_cmd_func_ ## Name(struct vxed* vxed, char* arg, void* ctx);

/* Seth vxl_accel_type sub-commands */
CMD_DECLARE__(set_vxl_accel_type_bvh)
CMD_DECLARE__(set_vxl_accel_type_svo)
CMD_DECLARE__(help_set_vxl_accel_type_bvh)
CMD_DECLARE__(help_set_vxl_accel_type_svo)

/* Set vxl_rt_coherency sub-commands */
CMD_DECLARE__(set_vxl_rt_coherency_high)
CMD_DECLARE__(set_vxl_rt_coherency_medium)
CMD_DECLARE__(set_vxl_rt_coherency_low)
CMD_DECLARE__(set_vxl_rt_coherency_mixed)
CMD_DECLARE__(set_vxl_rt_coherency_none)
CMD_DECLARE__(help_set_vxl_rt_coherency_high)
CMD_DECLARE__(help_set_vxl_rt_coherency_medium)
CMD_DECLARE__(help_set_vxl_rt_coherency_low)
CMD_DECLARE__(help_set_vxl_rt_coherency_mixed)
CMD_DECLARE__(help_set_vxl_rt_coherency_none)

#undef CMD_DECLARE__

#endif /* VXED_CMD_C_H */

