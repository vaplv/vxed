/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef VXED_CMD
  #define VXED_CMD(Name, Doc)
#endif

VXED_CMD(camera_speed_rotation, "Speed of the camera rotations.")
VXED_CMD(camera_speed_translation, "Speed of the camera translations.")
VXED_CMD(field_of_view, "Horizontal field of view (in degree.")
VXED_CMD(point_of_view, "Rendering point of view.")
VXED_CMD(print_load_perf, "Print the scene loading time performances.")
VXED_CMD(print_render_perf, "Print the rendering performances.")
VXED_CMD(video_mode, "Video mode of the rendering window.")
VXED_CMD(vxl_definition, "Number of voxels along the X, Y and Z axis.")
VXED_CMD(vxl_LOD_min, "Lower range of the octree depth to clamp access to.")
VXED_CMD(vxl_LOD_max, "Upper range of the octree depth to clamp access to.")
VXED_CMD(vxl_rt_coherency, "Level of coherency of the ray tracing.")
VXED_CMD(vxl_rt_simd, "Use SIMD accelerated ray tracer.")
VXED_CMD(vxr_beam_prepass, "Use the beam prepass ray-casting optimisation.")
VXED_CMD(vxr_render_type, "Type of image to draw.")

#undef VXED_CMD
