/* Copyright (C) 2014-2018 Vincent Forest (vaplv@free.fr)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "vxed.h"
#include "vxed_scene.h"

#include <rsys/clock_time.h>
#include <rsys/ref_count.h>
#include <rsys/str.h>

#include <vx/vxaw.h>
#include <vx/vxl.h>

struct vxed_scene {
  struct str filename;
  struct vxl_scene* vxl_scn;

  ref_T ref;
  struct vxed* ed;
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static void
vxed_scene_release(ref_T* ref)
{
  struct vxed_scene* scn = CONTAINER_OF(ref, struct vxed_scene, ref);
  ASSERT(ref);

  vxed_scene_clear(scn);
  str_release(&scn->filename);
  MEM_RM(scn->ed->allocator, scn);
}

/*******************************************************************************
 * Local functions
 ******************************************************************************/
VXED_DEFINE_REF_FUNCS(vxed_scene)

res_T
vxed_scene_create
  (struct vxed* ed,
   struct vxed_scene** out_scn)
{
  struct vxed_scene* scn = NULL;
  res_T res = RES_OK;
  ASSERT(ed && out_scn);

  scn = MEM_CALLOC(ed->allocator, 1, sizeof(struct vxed_scene));
  if(!scn) {
    logger_print(&ed->logger, LOG_ERROR, "Could not create the scene\n");
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&scn->ref);
  str_init(ed->allocator, &scn->filename);
  scn->ed = ed;

exit:
  *out_scn = scn;
  return res;
error:
  if(scn) {
    vxed_scene_ref_put(scn);
    scn = NULL;
  }
  goto exit;
}

res_T
vxed_scene_load
  (struct vxed_scene* scn,
   const size_t definition,
   const enum vxl_mem_location memloc,
   const char* filename)
{
  res_T res = RES_OK;
  ASSERT(scn && filename);

  vxed_scene_clear(scn);

  res = str_set(&scn->filename, filename);
  if(res != RES_OK) {
    logger_print(&scn->ed->logger, LOG_ERROR,
      "Could not set the model filename `%s'.\n", filename);
    goto error;
  }

  if(!scn->ed->options[OPT_PRINT_LOAD_PERF]) {
    res = vxaw_load
      (scn->ed->vxaw, definition, memloc, filename, NULL, &scn->vxl_scn);
    if(res != RES_OK) goto error;
  } else {
    struct vxaw_report report;
    char buf[64];
    const int mask = TIME_MIN | TIME_SEC | TIME_MSEC;
    struct time total;

    res = vxaw_load
      (scn->ed->vxaw, definition, memloc, filename, &report, &scn->vxl_scn);
    if(res != RES_OK) goto error;

    time_add(&total, &report.load, &report.voxelization);
    time_add(&total, &total, &report.build);

    time_dump(&total, mask, NULL, buf, sizeof(buf));
    logger_print(&scn->ed->logger, LOG_OUTPUT,
      "Load scene %s in %s:\n", filename, buf);
    time_dump(&report.load, mask, NULL, buf, sizeof(buf));
    logger_print(&scn->ed->logger, LOG_OUTPUT,
      "  Loading data in %s\n", buf);
    time_dump(&report.voxelization, mask, NULL, buf, sizeof(buf));
    logger_print(&scn->ed->logger, LOG_OUTPUT,
      "  Voxelization in %s\n", buf);
    time_dump(&report.build, mask, NULL, buf, sizeof(buf));
    logger_print(&scn->ed->logger, LOG_OUTPUT,
      "  Building SVO in %s\n", buf);
  }

exit:
  return res;
error:
  vxed_scene_clear(scn);
  goto exit;
}

struct vxl_scene*
vxed_scene_get_vxl_scene(const struct vxed_scene* scn)
{
  ASSERT(scn);
  return scn->vxl_scn;
}

void
vxed_scene_clear(struct vxed_scene* scn)
{
  ASSERT(scn);
  if(scn->vxl_scn) {
    VXL(scene_ref_put(scn->vxl_scn));
    scn->vxl_scn = NULL;
  }
  str_clear(&scn->filename);
}

